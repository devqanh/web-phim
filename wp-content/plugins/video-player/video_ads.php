﻿<?php
header("Content-type: text/xml");

echo '<?xml version="1.0"?>
<VAST version="2.0">
  <Ad id="static">
    <InLine>
      <AdSystem>Static VAST Template</AdSystem>
      <AdTitle>Static VAST Tag</AdTitle>
      <Impression>//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Impression>
      <Creatives>
        <Creative sequence="1">
          <Linear>
            <Duration>00:00:30</Duration>
            <TrackingEvents>
              <Tracking event="start">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="firstQuartile">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="midpoint">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="thirdQuartile">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="complete">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="pause">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="mute">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
              <Tracking event="fullscreen">//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</Tracking>
            </TrackingEvents>
            <VideoClicks>
              <ClickThrough>'.base64_decode($_GET['click']).'</ClickThrough>
              <ClickTracking>//s3.amazonaws.com/demo.jwplayer.com/player-demos/assets/pixel.gif</ClickTracking>
            </VideoClicks>
            <MediaFiles>
              <MediaFile type="video/mp4" bitrate="300" width="854" height="480">
				'.base64_decode($_GET['videoad']).'
              </MediaFile>
            </MediaFiles>
          </Linear>
        </Creative>
      </Creatives>
    </InLine>
  </Ad>
</VAST>';
?>