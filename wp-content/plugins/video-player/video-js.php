<?php
/*
Plugin Name: Video Player For WordPress
Plugin URI: http://cinehdbox.com
Description: Plugin giúp play video từ Photos, Drive và link .mp4 cố định sử dụng JWPlayer.
Author: Lê Thành Đạt
Author URI: https://fb.com/datmaster1991
Version: 2.4
Contact: https://fb.com/datmaster1991
Donate: https://fb.com/datmaster1991
*/

$plugin_dir = plugin_dir_path( __FILE__ );

/* The options page */
include_once($plugin_dir . 'admin.php');

/* Useful Functions */
include_once($plugin_dir . 'lib.php');

function curl($url){
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	$head[] = "Connection: keep-alive";
	$head[] = "Keep-Alive: 300";
	$head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
	$head[] = "Accept-Language: en-us,en;q=0.5";
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
	curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	$page = curl_exec($ch);
	curl_close($ch);
	return $page;
}

function cut_str($str, $left, $right){
	$str = substr(stristr($str, $left) , strlen($left));
	$leftLen = strlen(stristr($str, $right));
	$leftLen = $leftLen ? -($leftLen) : strlen($str);
	$str = substr($str, 0, $leftLen);
	return $str;
}

/* The [video] or [videojs] shortcode */
function video_shortcode($atts, $content=null){
	//add_videojs_header();
	
	$options = get_option('videojs_options'); //load the defaults
	
	extract(shortcode_atts(array(
		'mp4' => '',
		'google' => '',
		//'webm' => '',
		//'ogg' => '',
		'youtube' => '',
		'other' => '',
		'poster' => '',
		'subtitle' => '',
		'jwplayer_js' => $options['jwplayer_js'],
		'jwplayer_key' => $options['jwplayer_key'],
		'width' => $options['videojs_width'],
		'height' => $options['videojs_height'],
		'skin_jwplayer' => $options['skin_jwplayer'],
		'logo_jwplayer' => $options['logo_jwplayer'],
		'about_jwplayer' => $options['about_jwplayer'],
		'responsive' => $options['videojs_responsive'],
		//'preload' => $options['videojs_preload'],
		'autoplay' => $options['videojs_autoplay'],
		'poster_default' => $options['videojs_poster_default'],
		'server' => $options['videojs_server'],
		'key' => $options['videojs_key'],
		//'about_text' => $options['videojs_about_text'],
		//'about_link' => $options['videojs_about_link'],
		'video_ads' => $options['videojs_video_ads'],
		//'loop' => '',
		//'controls' => '',
		'id' => '',
		//'class' => '',
		//'muted' => ''
	), $atts));

	$dataSetup = array();
	
	// ID is required for multiple videos to work
	if ($id == '')
		$id = 'jwplayer-'.rand();

	// MP4 Source Supplied
	$mp4 = str_replace("&amp;","&",$mp4);
	if ($mp4)
		$mp4_source = 'sources: [{file:"'.$mp4.'",label:"360p","type":"mp4","default": "true"}],';
	else
		$mp4_source = '';

	// Google Source Supplied
	if ($google.$other.$youtube)
		if ($server==1){
			if($google){
				$curTemp = curl('http://www.apivn.net/ltp/?key='.$key.'&url='.$google);
				if(strpos($curTemp,'"file":')){
					$google_source = $curTemp;
					$google_source = 'sources: '.$google_source.',';
				}else if($curTemp=='[]'){
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($youtube){
				$curTemp = curl('http://www.apivn.net/getlink/?key='.$key.'&url='.$youtube);
				if(strpos($curTemp,'"file":')){
					$youtube_source = $curTemp;
					$youtube_source = 'sources: '.$youtube_source.',';
				}else if($curTemp=='[]'){
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($other){
				$curTemp = curl('http://www.apivn.net/getlink/?key='.$key.'&url='.$other);
				if(strpos($curTemp,'"file":')){
					$other_source = $curTemp;
					$other_source = 'sources: '.$other_source.',';
				}else if($curTemp=='[]'){
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}
		}
		
		elseif ($server==2){
			if($google){
				$google = str_replace('docs.google.com','drive.google.com', $google);
				$curTemp = curl("https://videoapi.io/api/getlink?key=".$key."&link=".$google."&return=full");
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'"sources":[',']},');
					$google_source = $curTemp;
					$google_source = str_replace('\/','/', $google_source);
					$google_source = 'sources: ['.$google_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($youtube){
				$curTemp = curl("https://videoapi.io/api/getlink?key=".$key."&link=".$youtube."&return=full");
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'"sources":[',']},');
					$youtube_source = $curTemp;
					$youtube_source = str_replace('\/','/', $youtube_source);
					$youtube_source = 'sources: ['.$youtube_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($other){
				$curTemp = curl("https://videoapi.io/api/getlink?key=".$key."&link=".$other."&return=full");
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'"sources":[','],');
					$other_source = $curTemp;
					$other_source = 'sources: ['.$other_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}
		}
		
		elseif ($server==3){
			if($google){
				$curTemp = curl($key.base64_encode($google));
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'|sources:[',']|');
					$google_source = $curTemp;
					$google_source = 'sources: ['.$google_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($youtube){
				$curTemp = curl($key.base64_encode($youtube));
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'|sources:[',']|');
					$youtube_source = $curTemp;
					$youtube_source = 'sources: ['.$youtube_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$youtube_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}else if($other){
				$curTemp = curl($key.base64_encode($other));
				if(strpos($curTemp,'"status":1')){
					$curTemp = cut_str($curTemp,'|sources:[',']|');
					$other_source = $curTemp;
					$other_source = 'sources: ['.$other_source.'],';
				}else if(strpos($curTemp,'"status":0')){
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
				}else{
					$other_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/not-server.mp4",label:"360p","type":"mp4","default": "true"}],';
				}
			}
		}
		
		elseif ($server==4){
			if(strpos($google,'photos.google.com')){
			$curTemp = curl($google);
			if(strpos($curTemp,'"79468658":')){
				$curTemp = cut_str($curTemp,'{"79468658":[[','"]');
				$curTemp = str_replace('\u003d','=', $curTemp);
				$curTemp = str_replace('\u0026','&', $curTemp);
				$curTemp = urldecode($curTemp);
				if ($curTemp <> "") {
					$curList = explode("&",$curTemp);
					foreach ($curList as $curl) {
					$curl = trim(substr($curl, strpos($curl,'https')-strlen($curl)));
						if ($curl <> "" ){
							if (strpos($curl,'itag=37') || strpos($curl,'=m37') !== false) {$v1080p=$curl;}
							if (strpos($curl,'itag=22') || strpos($curl,'=m22') !== false) {$v720p=$curl;}
							if (strpos($curl,'itag=59') || strpos($curl,'=m59') !== false) {$v480p=$curl;}
							if (strpos($curl,'itag=18') || strpos($curl,'=m18') !== false) {$v360p=$curl;}
						}
						
						if($v1080p){
							$google_source = 'sources: [{file:"'.$v1080p.'",label:"1080p","type":"mp4"},{file:"'.$v720p.'",label:"720p","type":"mp4"},{file:"'.$v480p.'",label:"480p","type":"mp4"},{file:"'.$v360p.'",label:"360p","type":"mp4"},{file:"'.$v360p.'",label:"Auto","type":"mp4","default": "true"}],';
						} 
		
						elseif($v720p){
							$google_source = 'sources: [{file:"'.$v720p.'",label:"720p","type":"mp4"},{file:"'.$v480p.'",label:"480p","type":"mp4"},{file:"'.$v360p.'",label:"360p","type":"mp4"},{file:"'.$v360p.'",label:"Auto","type":"mp4","default": "true"}],';
						} 
		
						elseif($v480p){
							$google_source = 'sources: [{file:"'.$v480p.'",label:"480p","type":"mp4"},{file:"'.$v360p.'",label:"360p","type":"mp4"},{file:"'.$v360p.'",label:"Auto","type":"mp4","default": "true"}],';
						}
		
						elseif($v360p){
							$google_source = 'sources: [{file:"'.$v360p.'",label:"360p","type":"mp4"},{file:"'.$v360p.'",label:"Auto","type":"mp4","default": "true"}],';
						}
		
						else {
							$google_source = 'sources: [{file:"'.$v360p.'",label:"Auto","type":"mp4","default": "true"}],';
						}
					}
				}
			}
			
			if(strpos($curTemp,'"117194011":')){
				$google_source = 'sources: [{file:"'.WP_PLUGIN_URL.'/video-player/error.mp4",label:"360p","type":"mp4","default": "true"}],';
			}
		}
	}
	
	else
		$google_source = '';
	
	// Poster image supplied
	if ($poster)//{
		$poster_attribute = $poster;
	//}
	else
		$poster_attribute = $poster_default;
	
	if($about_jwplayer){
		if(strpos($about_jwplayer,'|')){
			$about_jwplayer = explode("|",$about_jwplayer);
			$about_text = 'abouttext:"'.$about_jwplayer[0].'",';
			$about_link = 'aboutlink:"'.$about_jwplayer[1].'",';
		}else{
			$about_text = 'abouttext:"'.$about_jwplayer.'",';
		}
	}else{
		$about_text = 'abouttext:"Cine HD Box - Xem Phim HD Miễn Phí Tốc Độ Cao",';
		$about_link = 'aboutlink:"http://cinehdbox.com",';
	}
	
	if($skin_jwplayer){
		if(strpos($skin_jwplayer,'|')){
			$skin_jwplayer = explode("|",$skin_jwplayer);
			$skin_jwplayer = 'skin: {name: "'.$skin_jwplayer[0].'",url: "'.$skin_jwplayer[1].'"},';
		}else{
			$skin_jwplayer = 'skin: "'.$skin_jwplayer.'",';
		}
	}else{
		$skin_jwplayer = 'skin: "",';
	}
	
	if($logo_jwplayer){
		if(strpos($logo_jwplayer,'|')){
			$logo_jwplayer = explode("|",$logo_jwplayer);
			$logo_jwplayer = 'logo: {file: "'.$logo_jwplayer[0].'",link: "'.$logo_jwplayer[1].'"},';
		}else{
			$logo_jwplayer = 'logo: {file: "'.$logo_jwplayer.'"},';
		}
	}else{
		$logo_jwplayer = '';
	}
	
	if($video_ads){
		preg_match('/(.*)\|(.*)\|(.*)\|(.*)/', $video_ads, $link_video_ads);
			if ($link_video_ads[1]==1)
				$offset = "pre";
			if ($link_video_ads[1]==2)
				$offset = "post";
			if ($link_video_ads[1]>2)
				$offset = $link_video_ads[1];
			
			$ads_video_tag = plugin_dir_url( __FILE__ ).'video_ads.php?videoad='.base64_encode($link_video_ads[3]).'&click='.base64_encode($link_video_ads[4]);
			$videoads = 'advertising: {
				client: \'vast\',
				\'skipoffset\': '.$link_video_ads[2].',
				schedule: {"myAds":{"offset":"'.$offset.'","tag":"'.$ads_video_tag.'"}},
				admessage: "Quảng cáo sẽ đóng sau xx giây.",
				skipmessage: "Bỏ qua quảng cáo sau xx giây.",
				skiptext: "Bỏ qua quảng cáo",
			},';
	}else{
		$videoads = '';
	}

	// Subtitle supplied
	$subtitle = str_replace('https://www.dropbox.com','https://dl.dropboxusercontent.com', $subtitle);
	if ($subtitle)
		$subtitle_attribute = ' subtitle="'.$subtitle.'"';
	else
		$subtitle_attribute = '';

	$subtitle_color = $options['videojs_color_one'];
	
	if($jwplayer_js)
		$jwplayer_js = '<script src="'.$jwplayer_js.'"></script>';
	else
		$jwplayer_js = '<script src="https://content.jwplatform.com/libraries/KFOPxG1f.js"></script>';
	
	if($jwplayer_key)
		$jwplayer_key = '<script type="text/javascript">jwplayer.key="'.$jwplayer_key.'";</script>';
	else
		$jwplayer_key = '<script type="text/javascript">jwplayer.key="AQdC2L5nEyNNw3sYL/R2K+Qgx+cj47GmcBihMw==";</script>';

	//Output the <video> tag
	$videojs = <<<_end_

	<!-- Star Player v2.3 -->
	{$jwplayer_js}{$jwplayer_key}
	<div id="{$id}"></div>
	<script type="text/javascript">
		jwplayer("{$id}").setup({
			{$mp4_source}{$google_source}{$other_source}{$youtube_source}
			{$skin_jwplayer}
			{$logo_jwplayer}
			autostart: "{$autoplay_attribute}",
			image: "{$poster_attribute}",
			width: "{$width}",
			height: "{$height}",
			aspectratio: "{$responsive}",
			primary: "html5",
			tracks: [{
			file: "{$subtitle}",
			label: "Vietnamese",
			kind:  "captions",
			default: "true",
			}],
			captions: {
			color: "{$subtitle_color}",
			fontSize: 18,
			edgeStyle: "uniform",
			backgroundOpacity: 0,
			},
			{$about_text}{$about_link}
			{$videoads}
			});
	</script>
	<!-- End Player v2.3 -->

_end_;

	return $videojs;

}
add_shortcode('video', 'video_shortcode');
//Only use the [video] shortcode if the correct option is set
$options = get_option('videojs_options');
if( !array_key_exists('videojs_video_shortcode', $options) || $options['videojs_video_shortcode'] ){
	add_shortcode('video', 'video_shortcode');
}

/* TinyMCE Shortcode Generator */
function video_js_button() {
	if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
		return;
	if ( get_user_option('rich_editing') == 'true' ) {
		add_filter('mce_external_plugins', 'video_js_mce_plugin');
		add_filter('mce_buttons', 'register_video_js_button');
	}
}
add_action('init', 'video_js_button');

function register_video_js_button($buttons) {
	array_push($buttons, "|", "videojs");
	$options = get_option('videojs_options');
	echo('<div style="display:none"><input type="hidden" id="videojs-autoplay-default" value="' . $options['videojs_autoplay'] . '"><input type="hidden" id="videojs-preload-default" value="' . $options['videojs_preload'] . '"></div>'); //the default values from the admin screen, to be used by our javascript
	return $buttons;
}

function video_js_mce_plugin($plugin_array) {
	$plugin_array['videojs'] = plugins_url( 'mce-button.js?v=2.3' , __FILE__ );
	return $plugin_array;
}

?>