<?php

function videojs_enqueue_color_picker() {
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_script( 'videojs-admin', plugins_url('admin.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
add_action('load-settings_page_videojs-settings', 'videojs_enqueue_color_picker');

function videojs_menu() {
	global $videojs_admin;
	$videojs_admin = add_options_page('Video Player Settings', 'Video Player', 'manage_options', 'videojs-settings', 'videojs_settings');
}
add_action('admin_menu', 'videojs_menu');

/* Contextual Help */
function videojs_help($contextual_help, $screen_in, $screen) {
	global $videojs_admin;
	if ($screen_in == $videojs_admin) {
		$contextual_help = '';
	}
	return $contextual_help;
}
add_filter('contextual_help', 'videojs_help', 10, 3);


function videojs_settings() {
	if (!current_user_can('manage_options'))  {
		wp_die( __('Bạn không có đủ quyền truy cập vào trang này.') );
	}
	?>
	<div class="wrap">
	<h2>Video Player Settings</h2>
	
	<form method="post" action="options.php">
	<?php
	settings_fields( 'videojs_options' );
	do_settings_sections( 'videojs-settings' );
	?>
	<p class="submit">
	<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
	</p>
	<!--<h2>Using Video.js</h2>-->
	<?php
		echo file_get_contents(plugin_dir_path( __FILE__ ) . 'help.html');
		//echo "";
	?>
	</form>
	</div>
	<?php
	
}
add_action('admin_init', 'register_videojs_settings');

function register_videojs_settings() {
	register_setting('videojs_options', 'videojs_options', 'videojs_options_validate');
	add_settings_section('videojs_defaults', '', 'defaults_output', 'videojs-settings');
	add_settings_field('jwplayer_js', 'Link file jwplayer.js', 'jwplayer_js_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('jwplayer_key', 'Key JWPlayer', 'jwplayer_key_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_width', 'Chiều ngang (% hoặc px)', 'width_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_height', 'Chiều cao (% hoặc px)', 'height_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_responsive', 'Responsive', 'responsive_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('skin_jwplayer', 'Skin JWPlayer', 'skin_jwplayer_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('logo_jwplayer', 'Logo Player', 'logo_jwplayer_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('about_jwplayer', 'About Player', 'about_jwplayer_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_autoplay', 'Tự động phát', 'autoplay_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_color_one', 'Màu chữ phụ đề rời', 'color_one_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_poster_default', 'Poster mặc định', 'poster_default_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_server', 'Server trung gian', 'server_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_key', 'Key get link', 'key_output', 'videojs-settings', 'videojs_defaults');
	//add_settings_field('videojs_about_text', 'About text JWPlayer', 'about_text_output', 'videojs-settings', 'videojs_defaults');
	//add_settings_field('videojs_about_link', 'About link JWPlayer', 'about_link_output', 'videojs-settings', 'videojs_defaults');
	add_settings_field('videojs_video_ads', 'Quảng cáo video', 'video_ads_output', 'videojs-settings', 'videojs_defaults');
}

/* Validate our inputs */
function videojs_options_validate($input) {
	$newinput['jwplayer_js'] = $input['jwplayer_js'];
	$newinput['jwplayer_key'] = $input['jwplayer_key'];
	$newinput['videojs_height'] = $input['videojs_height'];
	$newinput['videojs_width'] = $input['videojs_width'];
	$newinput['skin_jwplayer'] = $input['skin_jwplayer'];
	$newinput['logo_jwplayer'] = $input['logo_jwplayer'];
	$newinput['about_jwplayer'] = $input['about_jwplayer'];
	$newinput['videojs_responsive'] = $input['videojs_responsive'];
	$newinput['videojs_autoplay'] = $input['videojs_autoplay'];
	$newinput['videojs_color_one'] = $input['videojs_color_one'];
	$newinput['videojs_poster_default'] = $input['videojs_poster_default'];
	$newinput['videojs_server'] = $input['videojs_server'];
	$newinput['videojs_key'] = $input['videojs_key'];
	$newinput['videojs_about_text'] = $input['videojs_about_text'];
	$newinput['videojs_about_link'] = $input['videojs_about_link'];
	$newinput['videojs_video_ads'] = $input['videojs_video_ads'];
	
	if(!preg_match("(.*?)", trim($newinput['jwplayer_js']))) {
		 $newinput['jwplayer_js'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['jwplayer_key']))) {
		 $newinput['jwplayer_key'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_width']))) {
		 $newinput['videojs_width'] = '';
	}
	 
	if(!preg_match("(.*?)", trim($newinput['videojs_height']))) {
		 $newinput['videojs_height'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['skin_jwplayer']))) {
		 $newinput['skin_jwplayer'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['logo_jwplayer']))) {
		 $newinput['logo_jwplayer'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['about_jwplayer']))) {
		 $newinput['about_jwplayer'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_responsive']))) {
		 $newinput['videojs_responsive'] = '';
	}
	 
	if(!preg_match("/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/", trim($newinput['videojs_color_one']))) {
		 $newinput['videojs_color_one'] = '#FFFF00';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_poster_default']))) {
		 $newinput['videojs_poster_default'] = '';
	}
	
	if(!preg_match("/^[1-9]*$/", trim($newinput['videojs_server']))) {
		 $newinput['videojs_server'] = '4';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_key']))) {
		 $newinput['videojs_key'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_about_text']))) {
		 $newinput['videojs_about_text'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_about_link']))) {
		 $newinput['videojs_about_link'] = '';
	}
	
	if(!preg_match("(.*?)", trim($newinput['videojs_video_ads']))) {
		 $newinput['videojs_video_ads'] = '';
	}
	return $newinput;
}

/* Display the input fields */

function defaults_output() {
	echo '';
}

function jwplayer_js_output() {
	$options = get_option('videojs_options');
	echo "<input id='jwplayer_js' name='videojs_options[jwplayer_js]' class='regular-text' type='text' placeholder='http://domain.com/jwplayer.js' value='{$options['jwplayer_js']}' />";
}

function jwplayer_key_output() {
	$options = get_option('videojs_options');
	echo "<input id='jwplayer_key' name='videojs_options[jwplayer_key]' class='regular-text' type='text' placeholder='Nếu bạn có key riêng jwplayer thì điền vào' value='{$options['jwplayer_key']}' />";
}

function width_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_width' name='videojs_options[videojs_width]' class='regular-text' type='text' placeholder='Ví dụ: 100%, 1280px' value='{$options['videojs_width']}' />";
}

function height_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_height' name='videojs_options[videojs_height]' class='regular-text' type='text' placeholder='Ví dụ: 480px' value='{$options['videojs_height']}' />";
}

function skin_jwplayer_output() {
	$options = get_option('videojs_options');
	echo "<input id='skin_jwplayer' name='videojs_options[skin_jwplayer]' class='regular-text' type='text' placeholder='Tên skin | Link skin' value='{$options['skin_jwplayer']}' />";
}

function logo_jwplayer_output() {
	$options = get_option('videojs_options');
	echo "<input id='logo_jwplayer' name='videojs_options[logo_jwplayer]' class='regular-text' type='text' placeholder='Link file logo .png | Link khi click vào logo' value='{$options['logo_jwplayer']}' />";
}

function about_jwplayer_output() {
	$options = get_option('videojs_options');
	echo "<input id='about_jwplayer' name='videojs_options[about_jwplayer]' class='regular-text' type='text' placeholder='About text | About link' value='{$options['about_jwplayer']}' />";
}

function responsive_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_responsive' name='videojs_options[videojs_responsive]' class='regular-text' type='text' placeholder='Ví dụ: 16:9, 24:10, 4:3' value='{$options['videojs_responsive']}' />";
}

function autoplay_output() {
	$options = get_option('videojs_options');
	if($options['videojs_autoplay']) { $checked = ' checked="checked" '; } else { $checked = ''; }
	echo "<input ".$checked." id='videojs_autoplay' name='videojs_options[videojs_autoplay]' type='checkbox' />";
}

function color_one_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_color_one' name='videojs_options[videojs_color_one]' type='text' value='{$options['videojs_color_one']}' data-default-color='#FFFF00' class='videojs-color-field' />";
}

function poster_default_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_poster_default' name='videojs_options[videojs_poster_default]' class='regular-text' type='text' value='{$options['videojs_poster_default']}' />";
}

function server_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_server' name='videojs_options[videojs_server]' class='regular-text' type='text' value='{$options['videojs_server']}' />";
}

function key_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_key' name='videojs_options[videojs_key]' class='regular-text' type='text' value='{$options['videojs_key']}' />";
}

function about_text_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_about_text' name='videojs_options[videojs_about_text]' class='regular-text' type='text' value='{$options['videojs_about_text']}' />";
}

function about_link_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_about_link' name='videojs_options[videojs_about_link]' class='regular-text' type='text' value='{$options['videojs_about_link']}' />";
}

function video_ads_output() {
	$options = get_option('videojs_options');
	echo "<input id='videojs_video_ads' name='videojs_options[videojs_video_ads]' class='regular-text' type='text' value='{$options['videojs_video_ads']}' />";
}

/* Set Defaults */
register_activation_hook(plugin_dir_path( __FILE__ ) . 'video-js.php', 'add_defaults_fn');

function add_defaults_fn() {
	$tmp = get_option('videojs_options');
    if(($tmp['videojs_reset']=='on')||(!is_array($tmp))) {
		$arr = array("jwplayer_js"=>"","jwplayer_key"=>"","videojs_height"=>"100%","videojs_width"=>"100%","skin_jwplayer"=>"","logo_jwplayer"=>"","about_jwplayer"=>"","videojs_responsive"=>"16:9","videojs_preload"=>"","videojs_autoplay"=>"","videojs_cdn"=>"on","videojs_color_one"=>"#FFFF00","videojs_reset"=>"","videojs_server"=>"4","videojs_poster_default"=>"");
		update_option('videojs_options', $arr);
		update_option("videojs_db_version", "1.0");
	}
}


/* Plugin Updater */
function update_videojs() {
	$videojs_db_version = "1.0";
	
	if( get_option("videojs_db_version") != $videojs_db_version ) { //We need to update our database options
		$options = get_option('videojs_options');
		
		//Set the new options to their defaults
		$options['videojs_color_one'] = "#FFFF00";
		//$options['videojs_color_two'] = "#66A8CC";
		//$options['videojs_color_three'] = "#000";
		$options['videojs_video_shortcode'] = "on";
		
		update_option('videojs_options', $options);
		
		update_option("videojs_db_version", $videojs_db_version); //Update the database version setting
	}
}
add_action('admin_init', 'update_videojs');

?>
