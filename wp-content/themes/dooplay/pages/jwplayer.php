<?php 
/*
Template Name: DT - jwplayer
*/

if( isset($_GET['source']) and isset($_GET['id']) ) {
$id	= $_GET['id'];
$source = base64_decode( $_GET['source'] );
$abouttext = get_option('dt_jw_abouttext');
$skinname = get_option('dt_jw_skinname');
$skinactive = get_option('dt_jw_skinactive');
$skininactive = get_option('dt_jw_skininactive');
$skinbackground = get_option('dt_jw_skinbackground');
$jwlogo = get_option('dt_jw_logo');
$jwkey = get_option('dt_jw_key');
$jwlogoposit = get_option('dt_jw_logo_position');
$image = rand_images('imagenes', $id, 'w1000', true, true);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<script src="<?php echo DT_DIR_URI. '/assets'; ?>/jwplayer/jwplayer.js"></script>
	<script src="<?php echo DT_DIR_URI. '/assets'; ?>/jwplayer/provider.html5.js"></script>
	<script>jwplayer.key="<?php echo $jwkey; ?>";</script>
	<link rel="stylesheet" type="text/css" href="<?php echo DT_DIR_URI. '/assets'; ?>/jwplayer/skins/seven.css">
	<style>
	html,body{
		height: 100%;
		width: 100%;
		margin: 0;
		overflow: hidden;
	}
	</style>
</head>
<body>
	<div id="video"></div>
	<script type="text/JavaScript">
		var playerInstance = jwplayer("video");
		playerInstance.setup({
			image: '<?php echo esc_url($image); ?>',
			file: '<?php echo esc_url($source); ?>',
			mute: "false",
			autostart: "false",
			repeat: "false",
			abouttext: "<?php echo $abouttext; ?>",
			aboutlink: "<?php echo esc_url( home_url() ); ?>",
			height: "100%",
			width: "100%",
			stretching: "uniform",
			primary: "html5",
			flashplayer: "<?php echo DT_DIR_URI. '/assets'; ?>jwplayer/jwplayer.flash.swf",
			preload:"metadata",
			skin: {
				name: "<?php echo $skinname; ?>",
				active: "<?php echo $skinactive; ?>",
				inactive: "<?php echo $skininactive; ?>",
				background: "<?php echo $skinbackground; ?>"
			},
			logo: {
				file: "<?php echo $jwlogo; ?>",
				hide: "false",
				link: "<?php echo esc_url( home_url() ); ?>",
				margin: "15",
				position: "<?php echo $jwlogoposit; ?>",
			}
		});
	</script>
</body>
</html>
<?php } else { 
	_d('Access denied'); 
} ?>