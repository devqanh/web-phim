<?php 
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/

get_header(); ?>
<div class="module">
	<div class="content">
	<?php if($_GET['letter']=='true') {
		get_template_part('pages/letter'); 
	} else {
		get_template_part('pages/search'); 
	} ?> 
	<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
	</div>
	<?php get_template_part('inc/parts/sidebar'); ?>
</div>
<?php get_footer(); ?>