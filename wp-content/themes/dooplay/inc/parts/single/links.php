<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/
if(get_option('dt_activate_post_links') =='true') { ?>
<div class="box_links">
	<?php get_template_part('inc/parts/single/listas/links'); ?>
</div>
<?php } ?>