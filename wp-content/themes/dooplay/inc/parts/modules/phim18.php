<header>

<h2>Phim 18+</h2>

<span><?php if($url = get_option('dt_danh_muc_slug','danh-muc')) { ?><a href="<?php echo esc_url( home_url() ) .'/'.$url.'/phim-18/'; ?>" class="see-all"><?php _d('See all'); ?></a><?php } ?></span>

</header>

<div class="items">
	<?php $phim18 = new WP_Query(array(
		'post_type'=>'movies',
		'post_status'=>'publish',
		'tax_query' => array(
	array(
        'taxonomy' => 'dtdanhmuc',
		'field' => 'tag_ID',
		'terms' => '712'
      )
  ),
		'orderby' => 'ID',
		'order' => 'DESC',
		'posts_per_page'=> '15'));
	?>
	<?php while ($phim18->have_posts()) : $phim18->the_post(); ?>
	<?php get_template_part('inc/parts/item'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>


