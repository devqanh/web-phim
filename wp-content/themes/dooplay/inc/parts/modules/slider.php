<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/

$option = get_option('dt_slider_radom');
if($option == 'true') {
	$rand = 'rand';
} else {
	$rand = '';	
} ?>
<div id="slider-movies-tvshows" class="animation-1 slider">
<?php query_posts( array('post_type' => array('tvshows','movies'), 'showposts' => get_option('dt_slider_items','10'), 'orderby' => $rand, 'order' => 'desc')); ?>
<?php while ( have_posts() ) : the_post(); get_template_part('inc/parts/item_b'); endwhile; wp_reset_query(); ?>
</div>