<header>

<h2>Phim Bộ Mới Cập Nhật</h2>

<span><?php if($url = get_option('dt_danh_muc_slug','danh-muc')) { ?><a href="<?php echo esc_url( home_url() ) .'/'.$url.'/phim-bo/'; ?>" class="see-all"><?php _d('See all'); ?></a><?php } ?></span>

</header>

<div class="items">
	<?php $phimbo = new WP_Query(array(
		'post_type'=>'movies',
		'post_status'=>'publish',
		'tax_query' => array(
	array(
        'taxonomy' => 'dtdanhmuc',
		'field' => 'tag_ID',
		'terms' => '54'
      )
  ),
		'orderby' => 'ID',
		'order' => 'DESC',
		'posts_per_page'=> '15'));
	?>
	<?php while ($phimbo->have_posts()) : $phimbo->the_post(); ?>
	<?php get_template_part('inc/parts/item'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>


