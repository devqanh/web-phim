<header>

<h2>Phim Việt Nam</h2>

<span><?php if($url = get_option('dt_quoc_gia_slug','quoc-gia')) { ?><a href="<?php echo esc_url( home_url() ) .'/'.$url.'/viet-nam/'; ?>" class="see-all"><?php _d('See all'); ?></a><?php } ?></span>

</header>

<div class="items">
	<?php $phimvietnam = new WP_Query(array(
		'post_type'=>'movies',
		'post_status'=>'publish',
		'tax_query' => array(
	array(
        'taxonomy' => 'dtquocgia',
		'field' => 'tag_ID',
		'terms' => '44'
      )
  ),
		'orderby' => 'ID',
		'order' => 'DESC',
		'posts_per_page'=> '15'));
	?>
	<?php while ($phimvietnam->have_posts()) : $phimvietnam->the_post(); ?>
	<?php get_template_part('inc/parts/item'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>