<?php 
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/
if(get_option('dt_mm_random_order') == 'true') {
	$rand = 'rand';
} else {
	$rand = '';	
} ?>

<header>
<h2>Phim Đề Cử</h2>
<?php if( get_option('dt_mm_activate_slider') == 'true') { if(get_option('dt_mm_autoplay_slider') == 'true') { } else { ?>
<div class="nav_items_module">
  <a class="btn prev3"><i class="icon-caret-left"></i></a>
  <a class="btn next3"><i class="icon-caret-right"></i></a>
</div>
<?php } } ?>
<span><?php if($url = get_option('dt_danh_muc_slug','danh-muc')) { ?><a href="<?php echo esc_url( home_url() ) .'/'.$url.'/phim-de-cu/'; ?>" class="see-all"><?php _d('See all'); ?></a><?php } ?></span>
</header>
<div id="movload" class="load_modules"><?php _d('Loading..');?></div>
<div <?php if(get_option('dt_mm_activate_slider') == 'true') { echo 'id="dt-movies"'; } ?> class="items">
	<?php $phimdecu = new WP_Query(array(
		'post_type' =>'movies',
		'showposts' => get_option('dt_mm_number_items','15'),
		'post_status' =>'publish',
		'tax_query' => array(
	array(
        'taxonomy' => 'dtdanhmuc',
		'field' => 'tag_ID',
		'terms' => '55'
      )
  ),
		'orderby' => $rand,
		'order' => 'DESC',
		'posts_per_page' => '15'));
	?>
	<?php while ($phimdecu->have_posts()) : $phimdecu->the_post(); ?>
	<?php get_template_part('inc/parts/item'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>