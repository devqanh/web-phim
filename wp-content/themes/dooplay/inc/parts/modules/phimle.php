<header>

<h2>Phim Lẻ Mới Cập Nhật</h2>

<span><?php if($url = get_option('dt_danh_muc_slug','danh-muc')) { ?><a href="<?php echo esc_url( home_url() ) .'/'.$url.'/phim-le/'; ?>" class="see-all"><?php _d('See all'); ?></a><?php } ?></span>

</header>

<div class="items">
	<?php $phimle = new WP_Query(array(
		'post_type'=>'movies',
		'post_status'=>'publish',
		'tax_query' => array(
	array(
        'taxonomy' => 'dtdanhmuc',
		'field' => 'tag_ID',
		'terms' => '53'
      )
  ),
		'orderby' => 'ID',
		'order' => 'DESC',
		'posts_per_page'=> '15'));
	?>
	<?php while ($phimle->have_posts()) : $phimle->the_post(); ?>
	<?php get_template_part('inc/parts/item'); ?>
	<?php endwhile; wp_reset_query(); ?>
</div>


