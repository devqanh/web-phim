<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.1
*
*/

/* Register page
-------------------------------------------------------------------------------
*/
function dbmovies_page() {
     add_menu_page(
        __d('Importer Tool'),
        __d('DbMovies'),
        'manage_options',
        'dbmovies.org',
        'dbmovies_callback',
        'dashicons-share-alt'
    );
}

/* HTML dbmovies.org
-------------------------------------------------------------------------------
*/
function dbmovies_callback() { 
	get_template_part('inc/dt_dbmovies');
}

/* dbmovies Ajax
-------------------------------------------------------------------------------
*/
function dbmovies_assets() {
	if($_GET['page']=='dbmovies.org') {
		wp_enqueue_style('dt-importer-tool-styles', DT_DIR_URI .'/assets/css/dbmovies.21.css', '', DT_VERSION, 'all');
		wp_enqueue_script('dt-importer-tool-scripts', DT_DIR_URI. '/assets/js/dbmovies.js', array('jquery'), DT_VERSION, false );
		wp_localize_script('dt-importer-tool-scripts', 'DTapi', array(
			// Importar
			'dbm'				=>  dbapiurl,
			'dbmkey'			=>	dbmapidata('k'),
			'ajaxurl'			=>	admin_url('admin-ajax.php', 'relative'),
			'apk'				=>	dbmapidata('s'),
			// Mensajes
			'preresultado'		=> __d('Searching content, wait a moment...'),
			'preresultadolog'	=> __d('Searching and extracting data...'),
			'resultadolog'		=> __d('Data found, completed process!'),
			'resultadoerror'	=> __d('Error, no data...'),
			'agregandodatos'	=> __d('Adding data...'),
			'agregandodatoslog' => __d('Adding content to the database...'),
			'procesocompleto'	=> __d('Process completed!'),
			'postdataerror'		=> __d('Content could not be added!'),
			'queryserver'		=> __d('Server status query...'),
			'verificationsr'	=> __d('Verification completed!'),
			'loading'			=> __d('Loading...'),
			'getcontent'		=> __d('Get content'),
			'saving'			=> __d('Saving data ..'),
			'save'				=> __d('Save settings')
		) );
	}
}

/* Search All content
-------------------------------------------------------------------------------
*/
function dbm_search_all() {
	if ( current_user_can('manage_options')) {
		if( isset($_POST['search-all-nonce'] ) and wp_verify_nonce($_POST['search-all-nonce'], 'search-all') ) { 
			$query = $_POST['query'];
			$page = $_POST['page'];
			$type = $_POST['type'];
			// Resolver json Search
			$api	= dt_http_api( tmdburl. 'search/'.$type.'?api_key='. tmdbkey .'&language='. tmdblang .'&query='.$query.'&page='.$page );
			$data	= json_decode($api, TRUE);
			// resultados de la API
			$pagex = $data['page'];
			$total_results = $data['total_results'];
			$total_pages = $data['total_pages'];
			$results = $data['results'];
			$ct = array();
			$limit = count($results);
			$num = ($limit * $page - $limit + 1  );
			echo '<div class="dtlist">';
			echo '<h1>'. __d('Page').' <b>'.$pagex .'</b> '. __d('of').' <b>'.$total_pages.'</b> '.__d('there are').' <b>'.$total_results.'</b> '.__d('titles found').'</h1>';
			foreach($results as $ci) {
				$id		= $ct[] = $ci['id'];
				if($type == 'movie'){
					$title	= $ct[] = $ci['title'];
					$year	= $ct[] = new DateTime($ci['release_date']);
					$class  = 'a_import_imdb';
					$meta	= 'idtmdb';
				} elseif($type == 'tv') { 
					$title	= $ct[] = $ci['name'];
					$year	= $ct[] = new DateTime($ci['first_air_date']);
					$class  = 'a_import_tmdb';
					$meta	= 'ids';
				}
				$poster = $ct[] = $ci['poster_path'];
				// vericador
				global $wpdb;
				$consulta = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = '$meta' AND meta_value = '{$id}' ";
				$verificar = $wpdb->get_results( $consulta, OBJECT );

				echo '<article id="' . $id. '">';
				if($poster) {
					echo '<div class="img"><img src="https://image.tmdb.org/t/p/w45'.$poster.'"></div>';
				} else {
					echo '<div class="no_img"><span class="dashicons dashicons-no"></span></div>';
				}
				if ($verificar) {
					echo $num. ' <span class="imported">'.__d('imported').'</span> <strong><a href="http://themoviedb.org/'.$type.'/'. $id.'/" target="_blank">'.$title.'</a></strong> <i>'. $year->format(DT_TIME) .'</i>';
				} else {
					echo $num. ' <span><a class="'.$class.'" data-id="'. $id.'">'. __d('Import') .'</a></span> <strong><a href="http://themoviedb.org/'.$type.'/'. $id.'" target="_blank">'.$title.'</a></strong> <i>'. $year->format(DT_TIME).'</i>';
				}
				echo '</article>';
				$num++;

			}
			echo '</div>';
			echo '<a id="load_more_search">'. __d('Next page'). '</a>';
		}
	}
	die();
}
add_action('wp_ajax_dbm_search_all', 'dbm_search_all');
add_action('wp_ajax_nopriv_dbm_search_all', 'dbm_search_all');

/* Get Movies
-------------------------------------------------------------------------------
*/
function dbm_get_movies() {
	if ( current_user_can('manage_options')) {
		if( isset($_POST['send-imdb-nonce'] ) and wp_verify_nonce($_POST['send-imdb-nonce'], 'send-imdb') ) { 
			// Parametros
			$apiyear = $_POST['imdbyear'];
			$apipage = $_POST['imdbpage'];
			// Resolver json discover
			$api	= dt_http_api( tmdburl. 'discover/movie?api_key='. tmdbkey .'&language='. tmdblang .'&sort_by=popularity.desc&page='.$apipage.'&primary_release_year='.$apiyear );
			$data	= json_decode( $api, TRUE );
			// resultados de la API
			$page = $data['page'];
			$total_results = $data['total_results'];
			$total_pages = $data['total_pages'];
			$results = $data['results'];
			$ct = array();
			$limit = count($results);
			$num = ($limit * $page - $limit + 1  );
			echo '<h1>'. __d('Page').' <b>'.$page .'</b> '. __d('of').' <b>'.$total_pages.'</b> '.__d('there are').' <b>'.$total_results.'</b> '.__d('titles found').'</h1>';
			echo '<div class="dtlist">';
			foreach($results as $ci) {
				$id		= $ct[] = $ci['id'];
				$title	= $ct[] = $ci['title'];
				$year	= $ct[] = new DateTime($ci['release_date']);
				$poster = $ct[] = $ci['poster_path'];
				// vericador
				global $wpdb;
				$consulta = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'idtmdb' AND meta_value = '{$id}' ";
				$verificar = $wpdb->get_results( $consulta, OBJECT );
				echo '<article id="' . $id. '">';
				if($poster) {
					echo '<div class="img"><img src="https://image.tmdb.org/t/p/w45'.$poster.'"></div>';
				} else {
					echo '<div class="no_img"><span class="dashicons dashicons-no"></span></div>';
				}
				if ($verificar) {
					echo $num. ' <span class="imported">'.__d('imported').'</span> <strong><a href="http://themoviedb.org/movie/'. $id.'/" target="_blank">'.$title.'</a></strong> <i>'. $year->format(DT_TIME) .'</i>';
				} else {
					echo $num. ' <span><a class="a_import_imdb" data-id="'. $id.'">'. __d('Import') .'</a></span> <strong><a href="http://themoviedb.org/movie/'. $id.'" target="_blank">'.$title.'</a></strong> <i>'. $year->format(DT_TIME) .'</i>';
				}
				echo '</article>';
				$num++;
			}
			echo '</div>';
			echo '<a id="load_more_imdb_link">'.__d('Next page').'</a>';
		}
	}
	die();
}

/* Get TV Shows
-------------------------------------------------------------------------------
*/
function dbm_get_tv() {
		set_time_limit(30000);
		if ( current_user_can('manage_options')) {
			if( isset($_POST['send-tmdb-nonce'] ) and wp_verify_nonce($_POST['send-tmdb-nonce'], 'send-tmdb') ) { 
				// Parametros
				$apiyear	= $_POST['tmdbyear'];
				$apipage	= $_POST['tmdbpage'];
				// Resolver json Discover
				$api	= dt_http_api( tmdburl. 'discover/tv?api_key='. tmdbkey .'&language='. tmdblang .'&sort_by=popularity.desc&first_air_date_year='.$apiyear.'&page='.$apipage );
				$data	= json_decode( $api, TRUE );
				// resultados de la API
				if($data['sucess'] == '101') {
					echo '<div class="api_error">'. __d('In maintenance, try later'). '</div>';
				} else {
					$page = $data['page'];
					$total_results = $data['total_results'];
					$total_pages = $data['total_pages'];
					$results = $data['results'];
					$ct = array();
					echo '<h1>'. __d('Page').' <b>'.$page .'</b> '. __d('of').' <b>'.$total_pages.'</b> '.__d('there are').' <b>'.$total_results.'</b> '.__d('titles found').'</h1>';
					echo '<div class="dtlist">';
					$limit = count($results);
					$num = ($limit * $page - $limit + 1  );
					// Mostrar resultados
					foreach($results as $ci) {
						$id		= $ct[] = $ci['id'];
						$poster = $ct[] = $ci['poster_path'];
						$name	= $ct[] = $ci['name'];
						$year	= $ct[] = new DateTime($ci['first_air_date']);

						// vericador
						global $wpdb;
						$consulta = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'ids' AND meta_value = '{$id}' ";
						$verificar = $wpdb->get_results( $consulta, OBJECT );
						
						echo '<article id="' . $id. '">';
						if($poster) {
							echo '<div class="img"><img src="https://image.tmdb.org/t/p/w45'.$poster.'"></div>';
						} else {
							echo '<div class="no_img"><span class="dashicons dashicons-no"></span></div>';
						}
						if ($verificar) {
							echo $num. ' <span class="imported">'.__d('imported').'</span> <strong>'. $name .'</strong> <i>'. $year->format(DT_TIME) .'</i>';
						} else {
							echo $num. ' <span><a class="a_import_tmdb" data-id="'.$id.'">'.__d('Import').'</a></span> <strong><a href="http://themoviedb.org/tv/'.$id.'/" target="_blank">'.$name.'</a></strong> <i>'. $year->format(DT_TIME) .'</i>';
						}
						echo '</article>';
						$num++;
					}
					echo '</div>';
					echo '<a id="load_more_tmdb_link">'.__d('Next page').'</a>';
				}
			}
		}
	die();
}

/* dbmovies.org status
-------------------------------------------------------------------------------
*/
function dbm_status() {
	set_time_limit(30000);
	if ( current_user_can('manage_options')) {
		if( isset($_POST['send-status-nonce'] ) and wp_verify_nonce($_POST['send-status-nonce'], 'send-status') ) { 
			// Resolver json Discover
			$api	= dt_http_api( dbapiurl. '/status.php' );
			$data	= json_decode( $api, TRUE);
			// Resultados
			$status = $data['status'];
			$dbmovies = $data['process']['dbmovies'];
			$imdb = $data['process']['imdbscraper'];
			$tmdb = $data['process']['themoviedb'];
			$imdb1 = $data['count']['imdb1'];
			$imdb2 = $data['count']['imdb2'];

			if($dbmovies >= '76') { $color = '#da3b3b'; }
			if($dbmovies <= '75') { $color = '#f68b1f'; }
			if($dbmovies < '50') { $color = '#9bca3e'; }
			if($imdb >= '76') { $color1 = '#da3b3b'; }
			if($imdb <= '75') { $color1 = '#f68b1f'; }
			if($imdb < '50') { $color1 = '#9bca3e'; }
			if($tmdb >= '76') { $color2 = '#da3b3b'; }
			if($tmdb <= '75') { $color2 = '#f68b1f'; }
			if($tmdb < '50') { $color2 = '#9bca3e'; }
			if($status == '200') { 
				echo '
				<div class="skillbar clearfix" data-percent="'.$dbmovies.'%">
				<div class="skillbar-title"><span>dbmovies</span></div>
				<div class="skillbar-bar" style="background: '.$color.'"></div>
				<div class="skill-bar-percent">'.$dbmovies.'%</div>
				</div>

				<div class="skillbar clearfix" data-percent="'.$imdb.'%">
				<div class="skillbar-title"><span>imdbscraper</span></div>
				<div class="skillbar-bar" style="background: '.$color1.'"></div>
				<div class="skill-bar-percent">'.$imdb.'%</div>
				</div>

				<div class="skillbar clearfix" data-percent="'.$tmdb.'%">
				<div class="skillbar-title"><span>themoviedb</span></div>
				<div class="skillbar-bar" style="background: '.$color2.'"></div>
				<div class="skill-bar-percent">'.$tmdb.'%</div>
				</div>
				';
			} elseif($status == '403') {
				echo '<div class="api_error">'. __d('Access denied'). '</div>';
			} elseif($status == '101') {
				echo '<div class="api_error">'. __d('In maintenance, try later'). '</div>';
			} else {
				echo '<div class="api_error">'. __d('Server blocked'). '</div>';
			}
		}
	}
	die();
}

/* Post Movies
-------------------------------------------------------------------------------
*/
function dbm_post_movie() {
	set_time_limit(30000);
	if( isset($_POST['send-movies-nonce'] ) and wp_verify_nonce($_POST['send-movies-nonce'], 'send-movies') ) { 
		if (current_user_can('manage_options')) {
			$idmovie = $_POST["idmovie"];
			if (($idmovie != NULL)) {
				$json_1 = dt_http_api( tmdburl. "movie/" . $idmovie . "?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data = json_decode($json_1, TRUE);
				// ##########################################
				$imdb = $data['imdb_id'];
				$idtmdb = $data['id'];
				$site =  get_option("siteurl");
				$jsonn = dt_http_api( IMDB_API_URL. '/'. $imdb );
				$data1 = json_decode($jsonn, TRUE);
				// ##########################################
				$a4 = $data1['imdbRating'];
				$a5 = $data1['imdbVotes'];
				$a6 = $data1['Rated'];
				$a7 = $data1['Country'];
				// ##########################################
				$b1 = $data['runtime'];
				$b2 = $data['tagline'];
				$b3 = $data['title'];
				$b4 = $data['overview'];
				$b9 = $data['vote_count'];
				$b10 = $data['vote_average'];
				$b11 = $data['release_date'];
				$b12 = $data['original_title'];
				$a3 = substr($b11, 0, 4);
				$b13 = $data['poster_path'];
				if ($get_img = $data['poster_path'])
				{
					$upimg = 'https://image.tmdb.org/t/p/w396' . $get_img;
				}
				$b14 = $data['backdrop_path'];
				$b15 = $data['images']["backdrops"];
				$i = '0';
				foreach($b15 as $valor2) if ($i < 10) {
					$imgs.= $valor2['file_path'] . "\n";
					$i +=1;
				}
				$b16 = $data['genres'];
				$generos = array();
				foreach($b16 as $ci)
				{
					$generos[] = $ci['name'];
				}
				$b17 = 'mov'. DT_STRING_LINK. $data['id'];
				// ##########################################
				$json_2 = dt_http_api( tmdburl. "movie/" . $idmovie . "/credits?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data2 = json_decode($json_2, TRUE);
				// ##########################################
				$c1 = $data2['cast'];
				$i = '0';
				foreach($c1 as $valor) if ($i < 10) {
					$actores.= $valor['name'] . ",";
					$i +=1;
				}
				$i = '0';
				foreach($c1 as $valor) if ($i < 10) {
					if ($valor['profile_path'] == NULL)
					{
						$valor['profile_path'] = "null";
					}
					$d_actores.= "[" . $valor['profile_path'] . ";" . $valor['name'] . "," . $valor['character'] . "]";
					$i +=1;
				}
				$c2 = $data2['crew'];
				foreach($c2 as $valorc)
				{
					$departamente = $valorc['department'];
					if ($valorc['profile_path'] == NULL)
					{
						$valorc['profile_path'] = "null";
					}
					if ($departamente == "Directing")
					{
						$d_dir.= "[" . $valorc['profile_path'] . ";" . $valorc['name'] . "]";
					}
					if ($departamente == "Directing")
					{
						$dir.= $valorc['name'] . ",";
					}
				}
				// ##########################################
				$json_3 = dt_http_api( tmdburl. "movie/" . $idmovie . "/videos?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data3  = json_decode($json_3, TRUE);
				// ##########################################
				$d1 = $data3['results'];
				foreach($d1 as $yt)
				{
					$youtube.= "[" . $yt['key'] . "]";
					break;
				}
				// ##########################################
				$op_release_date = get_option('dt_api_release_date');
				if($op_release_date == 'true') {
					$my_post = array(
						'post_title' => dt_clear($b3),
						'post_content' => dt_clear($b4),
						'post_date'     => $b11,
						'post_date_gmt' => $b11,
						'post_status' => 'publish',
						'post_type' => 'movies',
						'post_author' => 1
					);
				} else {
					$my_post = array(
						'post_title' => dt_clear($b3),
						'post_content' => dt_clear($b4),
						'post_date'     => date('Y-m-d H:i:s'),
						'post_date_gmt' => date('Y-m-d H:i:s'),
						'post_status' => 'publish',
						'post_type' => 'movies',
						'post_author' => 1
					);
				}

				// vericador
				global $wpdb;
				$consulta = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'ids' AND meta_value = '{$imdb}' ";
				$verificar = $wpdb->get_results( $consulta, OBJECT );
				if ($verificar) {
					echo '<strong>ERROR:</strong> '. __d('content already exists');
				} else {
					$post_id = wp_insert_post($my_post);
					echo '<span class="import_completed">'. __d('imported').'</span> <a href="'. esc_url( home_url() ) .'?p='. $post_id .'" target="_blank"><strong>'. $b3 .'</strong></a> ('. $a3 .')';
				}	
				wp_set_post_terms($post_id, $dir, 'dtdirector', false);
				wp_set_post_terms($post_id, $a3, 'dtyear', false);
				wp_set_post_terms($post_id, $actores, 'dtcast', false);
				wp_set_object_terms($post_id, $generos, 'genres', false);
				add_post_meta($post_id, "ids", ($imdb) , true);
				add_post_meta($post_id, "idtmdb", ($idtmdb) , true);
				add_post_meta($post_id, "dt_poster", ($b13) , true);
				add_post_meta($post_id, "dt_backdrop", ($b14) , true);
				add_post_meta($post_id, "imagenes", ($imgs) , true);
				add_post_meta($post_id, "youtube_id", ($youtube) , true);
				add_post_meta($post_id, "imdbRating", ($a4) , true);
				add_post_meta($post_id, "imdbVotes", ($a5) , true);
				add_post_meta($post_id, "Rated", ($a6) , true);
				add_post_meta($post_id, "Country", ($a7) , true);
				add_post_meta($post_id, "original_title", ($b12) , true);
				add_post_meta($post_id, "release_date", ($b11) , true);
				add_post_meta($post_id, "vote_average", ($b10) , true);
				add_post_meta($post_id, "vote_count", ($b9) , true);
				add_post_meta($post_id, "tagline", ($b2) , true);
				add_post_meta($post_id, "runtime", ($b1) , true);
				add_post_meta($post_id, "dt_string", ($b17) , true);
				add_post_meta($post_id, "dt_cast", ($d_actores) , true);
				add_post_meta($post_id, "dt_dir", ($d_dir) , true);
				dt_upload_image($upimg, $post_id);
			}	
		}
	} 
	die();
}

/* Post TV Shows
-------------------------------------------------------------------------------
*/
function dbm_post_tv() {
	set_time_limit(30000);
	if( isset($_POST['send-series-nonce'] ) and wp_verify_nonce($_POST['send-series-nonce'], 'send-series') ) { 
		if (current_user_can('manage_options')) {
			$slug = "/" . get_option('dt_tvshows_slug', 'tvshows');
			$ids = $_POST["idtv"];
			if (($ids != NULL)) {
				$json2 = dt_http_api( tmdburl. "tv/" . $ids . "?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data2 = json_decode($json2, TRUE);
				// ##########################################
				$name = $data2['name'];
				$tvid = $data2['id'];
				$episodes = $data2['number_of_episodes'];
				$seasons = $data2['number_of_seasons'];
				$year = substr($data2['first_air_date'], 0, 4);
				$date1 = $data2['first_air_date'];
				$date2 = $data2['last_air_date'];
				$overview = $data2['overview'];
				$popularidad = $data2['popularity'];
				$originalname = $data2['original_name'];
				$promedio = $data2['vote_average'];
				$votos = $data2['vote_count'];
				$tipo = $data2['type'];
				$web = $data2['homepage'];
				$status = $data2['status'];
				$poster = $data2['poster_path'];
				if ($get_img = $data2['poster_path']) {
					$upload_poster = 'https://image.tmdb.org/t/p/w396' . $get_img;
				}
				$backdrop = $data2['backdrop_path'];
				// Forech!
				$i = '0';
				$images = $data2['images']["backdrops"];
				foreach($images as $valor2) if ($i < 10) {
					$imgs.= $valor2['file_path'] . "\n";
					$i +=1;
				}

				$genres = $data2['genres'];
				$generos = array();
				foreach($genres as $ci) {
					$generos[] = $ci['name'];
				}
				$networks = $data2['networks'];
				foreach($networks as $co) {
					$redes.= $co['name'];
				}
				$studio = $data2['production_companies'];
				foreach($studio as $ht) {
					$estudios.= $ht['name'] . ",";
				}
				$creator = $data2['created_by'];
				foreach($creator as $cg) {
					$creador.= $cg['name'] . ",";
				}
				foreach($creator as $ag) {
					if ($ag['profile_path'] == NULL) {
						$ag['profile_path'] = "null";
					}
					$creador_d.= "[" . $ag['profile_path'] . ";" . $ag['name'] . "]";
				}
				$runtime = $data2['episode_run_time'];
				foreach($runtime as $tm) {
					$duracion.= $tm;
					break;
				}
				// ##########################################
				$json3 = dt_http_api( tmdburl. "tv/" . $ids . "/credits?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data3 = json_decode($json3, TRUE);
				// ##########################################
				$cast = $data3['cast'];
				$i = '0';
				foreach($cast as $valor) if ($i < 10) {
					$actores.= $valor['name'] . ",";
					$i +=1;
				}
				$i = '0';
				foreach($cast as $valor) if ($i < 10) {
					if ($valor['profile_path'] == NULL) {
						$valor['profile_path'] = "null";
					}
					$d_actores.= "[" . $valor['profile_path'] . ";" . $valor['name'] . "," . $valor['character'] . "]";
					$i +=1;
				}
				// ##########################################
				$json4 = dt_http_api( tmdburl. "tv/" . $ids . "/videos?append_to_response=images,trailers&language=" . tmdblang . "&include_image_language=" . tmdblang . ",null&api_key=" . tmdbkey );
				$data4 = json_decode($json4, TRUE);
				// ##########################################
				$video = $data4['results'];
				foreach($video as $yt) {
					$youtube.= "[" . $yt['key'] . "]";
					break;
				}
				// ##########################################
				$op_release_date = get_option('dt_api_release_date');
				if($op_release_date == 'true') {
					$my_post = array(
						'post_title' => dt_clear($name),
						'post_content' => dt_clear($overview),
						'post_status' => 'publish',
						'post_type' => 'tvshows',
						'post_date'     => $date1,
						'post_date_gmt' => $date1,
						'post_author' => 1
					);
				} else {
					$my_post = array(
						'post_title' => dt_clear($name),
						'post_content' => dt_clear($overview),
						'post_status' => 'publish',
						'post_type' => 'tvshows',
						'post_date'     => date('Y-m-d H:i:s'),
						'post_date_gmt' => date('Y-m-d H:i:s'),
						'post_author' => 1
					);
				}	
				
				// vericador
				global $wpdb;
				$consulta = "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'ids' AND meta_value = '{$tvid}' ";
				$verificar = $wpdb->get_results( $consulta, OBJECT );
				if ($verificar) {
					echo '<strong>ERROR:</strong> '. __d('content already exists');
				} else {
					$post_id = wp_insert_post($my_post);
					echo '<span class="import_completed">'. __d('imported').'</span> <a href="'. esc_url( home_url() ) .'?p='. $post_id .'" target="_blank"><strong>'. $name .'</strong></a> ('. $year .')';
				}
				wp_set_post_terms($post_id, $year, 'dtyear', false);
				wp_set_object_terms($post_id, $generos, 'genres', false);
				wp_set_post_terms($post_id, $redes, 'dtnetworks', false);
				wp_set_post_terms($post_id, $estudios, 'dtstudio', false);
				wp_set_post_terms($post_id, $actores, 'dtcast', false);
				wp_set_post_terms($post_id, $creador, 'dtcreator', false);
				add_post_meta($post_id, "ids", ($tvid) , true);
				add_post_meta($post_id, "dt_poster", ($poster) , true);
				add_post_meta($post_id, "dt_backdrop", ($backdrop) , true);
				add_post_meta($post_id, "imagenes", ($imgs) , true);
				add_post_meta($post_id, "youtube_id", ($youtube) , true);
				add_post_meta($post_id, "first_air_date", ($date1) , true);
				add_post_meta($post_id, "last_air_date", ($date2) , true);
				add_post_meta($post_id, "number_of_episodes", ($episodes) , true);
				add_post_meta($post_id, "number_of_seasons", ($seasons) , true);
				add_post_meta($post_id, "original_name", ($originalname) , true);
				add_post_meta($post_id, "status", ($status) , true);
				add_post_meta($post_id, "imdbRating", ($promedio) , true);
				add_post_meta($post_id, "imdbVotes", ($votos) , true);
				add_post_meta($post_id, "episode_run_time", ($duracion) , true);
				add_post_meta($post_id, "dt_cast", ($d_actores) , true);
				add_post_meta($post_id, "dt_creator", ($creador_d) , true);
				dt_upload_image($upload_poster, $post_id);
			}
		}
	}
	die();
}

/* Generate API info
-------------------------------------------------------------------------------
*/
function dt_dbmovies_rest_api_info() {
	if( isset($_REQUEST['key']) ) {
		if( $_REQUEST['key'] == $_REQUEST['key'] ) {
			$tmdblang	= get_option('dt_api_language');
			$email		= get_option("admin_email");
			$key		= get_option( DT_KEY_S );
			$tmdbkey	= get_option('dt_api_key');
			$tmdbactive	= get_option('dt_activate_api');
			$upload		= get_option('dt_api_upload_poster');
			$genres		= get_option('dt_api_genres');
			$release	= get_option('dt_api_release_date');
			$ip			= $_SERVER['SERVER_ADDR'];
			$data = array(
				'response'	 => true,
				'dooplay'	 => array( 'version' => DT_VERSION,'license' => $key ),
				'dbmovies'   => array( 'status' => 'valid','domain'	=> dt_domain( get_bloginfo('siteurl')),'key' => dbmapidata('k')),
				'themoviedb' => array( 'active' => $tmdbactive,'key' => $tmdbkey, 'lang' => $tmdblang, 'genres' => $genres,'upload_image' => $upload, 'release_date' => $release ),
				'wordpress'	 => array( 'ip' => $ip, 'version' => get_bloginfo('version'), 'name' => get_bloginfo('name'), 'url' => dt_domain( get_bloginfo('siteurl') ), 'email'=> $email )
			);
		} else {
			$data = array('response' => false, 'error' => '102');
		}
	} else {
		$data = array('response' => false, 'error' => '103');
	}
	return $data;
}

/* APK
-------------------------------------------------------------------------------
*/
function dt_dbmovies_rest_api_apk() {
	if( isset($_REQUEST['key']) ) {
		if( $_REQUEST['key'] == $_REQUEST['key'] ) {
			$data = array(
				'response' => true,
				'message' => __d('data restored')
			);
			delete_option('_site_register_in_dbmvs'); 
			delete_option('dt_register_note'); 
		} else {
			$data = array(
				'response' => false,
				'message' => __d('ready to install')
			);
		}
	}
	return $data;
}

/* WP-JSON Rest API
-------------------------------------------------------------------------------
*/
function dt_dbmovies_register_api() {
	# Set 1
	register_rest_route('dbmvs', '/app/', array(
        'methods' => 'GET',
        'callback' => 'dt_dbmovies_rest_api_info',
    ));

	# Set 2
	register_rest_route('dbmvs', '/appd/', array(
        'methods' => 'GET',
        'callback' => 'dt_dbmovies_rest_api_apk',
    ));

	# Set 3
	register_rest_route('dbmvs', '/appe/', array(
		'methods' => 'GET',
		'callback' => 'dt_dbmovies_rest_api_ipk',
	));
}

/* Save dbmovies Settings
-------------------------------------------------------------------------------
*/
function save_dbmovies_settings() {
	if ( current_user_can('manage_options') ) {
		// Only IF
		if( isset($_POST['action']) && $_POST['action'] == "dbm_save_settings" ){

			// Options
			$lang = $_POST['dt_api_language'];
			$key = $_POST['dt_api_key'];
			$activeapi = $_POST['dt_activate_api'] ? 'true' : 'false';
			$apigenres = $_POST['dt_api_genres'] ? 'true' : 'false';
			$apiupload = $_POST['dt_api_upload_poster'] ? 'true' : 'false';
			$apirelease = $_POST['dt_api_release_date'] ? 'true' : 'false';
			
			// Update options
			update_option('dt_api_language', $lang );
			update_option('dt_api_key', $key);
			update_option('dt_activate_api', $activeapi);
			update_option('dt_api_genres', $apigenres);
			update_option('dt_api_upload_poster', $apiupload);
			update_option('dt_api_release_date', $apirelease);

			_d('Updated data');
		}
	}
	die();
}

/* All actions
-------------------------------------------------------------------------------
*/
add_action('admin_menu', 'dbmovies_page');
add_action('admin_enqueue_scripts', 'dbmovies_assets');
add_action('rest_api_init', 'dt_dbmovies_register_api');

/* Ajax actions
-------------------------------------------------------------------------------
*/
add_action('wp_ajax_dbm_status', 'dbm_status');
add_action('wp_ajax_dbm_get_movies', 'dbm_get_movies');
add_action('wp_ajax_dbm_get_tv', 'dbm_get_tv');
add_action('wp_ajax_dbm_post_movie', 'dbm_post_movie');
add_action('wp_ajax_dbm_post_tv', 'dbm_post_tv');
add_action('wp_ajax_dbm_save_settings', 'save_dbmovies_settings');

add_action('wp_ajax_nopriv_dbm_status', 'dbm_status');
add_action('wp_ajax_nopriv_dbm_get_movies', 'dbm_get_movies');
add_action('wp_ajax_nopriv_dbm_get_tv', 'dbm_get_tv');
add_action('wp_ajax_nopriv_dbm_post_movie', 'dbm_post_movie');
add_action('wp_ajax_nopriv_dbm_post_tv', 'dbm_post_tv');
add_action('wp_ajax_nopriv_dbm_save_settings', 'save_dbmovies_settings');