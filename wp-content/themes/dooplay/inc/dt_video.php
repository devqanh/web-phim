<?php 
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 3/04/2017 - 20:00:21
* @last modified by: Erick Meza
* @last modified time: 3/04/2017 - 20:00:21
*
*/


/* Video Assets
-------------------------------------------------------------------------------
*/
function dt_assets_video() {
	if(is_single()) {
		wp_enqueue_style('dt_player_style', DT_DIR_URI .'/assets/player/dist/plyr.css' , array(), DT_VERSION, 'all');
		wp_enqueue_script('dt_player_main_script',  DT_DIR_URI .'/assets/player/dist/plyr.js' , array(), DT_VERSION, true  );
		wp_enqueue_script('dt_player_script',  DT_DIR_URI .'/assets/player/setup.js' , array(), DT_VERSION, true  );
	}
}
add_action('wp_enqueue_scripts', 'dt_assets_video'); 

/* Video functions
-------------------------------------------------------------------------------
*/
function dt_video( $id, $url ) {
	$image = rand_images('imagenes', $id, 'original', true, true);
	echo '<section class="dt_player_video">';
	echo '<video class="dt_player" poster="'.$image.'" controls crossorigin>';
	echo '<source src="'.$url.'" type="video/mp4">';
	echo '<a href="'.$url.'">download></a>';
	echo '</video>';
	echo '</section>';
}