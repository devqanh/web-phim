<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.1
*
*/

$register	= get_option('dt_register_note');
$database	= get_option('dooplay_database');
$license	= dttp;

/* Notice Activate License
-------------------------------------------------------------------------------
*/
function activate_license() { 
    echo '<div class="notice notice-error is-dismissible"><p>';
	echo '<span class="dashicons dashicons-admin-network" style="color: #dc3232;"></span> ';
    echo __d('Invalid license, it is possible that some of the options may not work correctly'). ', '.'<a href="'. admin_url(). 'themes.php?page='. DT_THEME_SLUG. '-license"><strong>'. __d('here'). '</strong></a>';
    echo '</p></div>';
}

/* Register dbmovies API
-------------------------------------------------------------------------------
*/
function activate_dbmovies() { 
    echo '<div class="notice notice-error is-dismissible activate_dbmovies_true"><p id="ac_dbm_not">';
	echo '<span class="dashicons dashicons-warning" style="color: #dc3232;"></span> ';
	echo __d('dbmovies API has not been activated, active to be able to generate content'). ' <a data-register="true" class="activate_dbmovies"><strong>'. __d('click here to activate') .'</strong></a>';
    echo '</p></div>';
}

/* Notify update database
-------------------------------------------------------------------------------
*/
function dooplay_update() { 
    echo '<div class="notice notice-warning is-dismissible"><p id="cfg_dts">';
	echo '<span class="dashicons dashicons-update" style="color: #ffb900;"></span> ';
    echo __d('Dooplay requires you to update the database'). ' <a data-action="updatedb" class="dooplay_update_database"><strong>'. __d('click here to update') .'</strong></a>';;
    echo '</p></div>'; 
}

/* Logical comparators
-------------------------------------------------------------------------------
*/

// Notices Admin
if ( $license !== 'valid') {
	add_action( 'admin_notices', 'activate_license' );
}elseif ( $database !== '2.1' ) {
	add_action( 'admin_notices', 'dooplay_update' );
}elseif ( $register !== '1' ) {
	add_action( 'admin_notices', 'activate_dbmovies' );
}