<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.1
*
*/
add_action('admin_enqueue_scripts', 'dt_framework_scripts');
function dt_framework_scripts( $hook ) {
    if( is_admin() ) { if ( isset($_GET['page']) && $_GET['page'] == DT_THEME_SLUG ) {
			wp_enqueue_media();
			wp_enqueue_style('dt_framework_css', DT_DIR_URI .'/assets/admin/assets/css/acera_css.css' , array(), DT_VERSION, 'all');
			wp_enqueue_script('dt_main_js',  DT_DIR_URI .'/assets/admin/assets/js/mainJs.js' , array(), DT_VERSION, false );
			wp_enqueue_script('dt_color_picker', DT_DIR_URI. '/assets/admin/assets/js/colorpicker.js', array('jquery','wp-color-picker','thickbox'), false, true );
			wp_enqueue_style('wp-color-picker' );
			wp_enqueue_style('wp-jquery-ui-dialog' );
			wp_enqueue_script('wp-color-picker' );
			wp_enqueue_script('jquery-ui-dialog' );
			wp_enqueue_script('jquery-ui-sortable' );
			wp_enqueue_script('jquery-ui-accordion' );
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-form');
			wp_localize_script('dt_main_js', 'dtfa', array(
				// Importar
				'ajaxurl' => admin_url('admin-ajax.php', 'relative'),
				'ititle' => __d('Upload image'),
				'iuse' =>	__d('Use image'),
				'save' =>	__d('Settings saved.'),
				'remov' => __d('Removing..')
				
			) );
		}
    }
}
if (!function_exists('dt_upload_image_editor')) {
    function dt_upload_image_editor() { ?>
			<script type="text/javascript" src="<?php echo DT_DIR_URI ."/assets/js/upload_images.js"; ?>"></script>
        <?php
    }
}
