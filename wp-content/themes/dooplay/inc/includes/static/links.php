<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/

/* Page options
-------------------------------------------------------------------------------
*/
if (is_admin()) {
	include_once DT_DIR . '/inc/includes/static/inc/dt_options.php';
	include_once DT_DIR . '/inc/includes/static/inc/dt_assets.php';
	include_once DT_DIR . '/inc/includes/static/inc/dt_image_upload.php';
	include_once DT_DIR . '/inc/includes/static/inc/dt_page_options.php';
	new acera_theme_options($options);
	add_action('admin_head', 'dt_upload_image_editor');
}
