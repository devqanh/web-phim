<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.1
*
*/

function seasons_add_meta_box() {
	add_meta_box(
		'mt_metabox',
		__d('seasons Info'),
		'seasons_html',
		'seasons',
		'normal',
		'high'
	);
}
add_action('add_meta_boxes', 'seasons_add_meta_box'); function seasons_html( $post) { wp_nonce_field('_seasons_nonce', 'seasons_nonce'); ?>
<div id="loading_api"></div>
<div id="api_table">
	<table class="options-table-responsive dt-options-table">
		<tbody>
			<tr id="ids_box">
				<td class="label">
					<label><?php _d('Generate data'); ?></label>
					<p class="description"><?php _d('Generate data from <strong>themoviedb.org</strong>'); ?></p>
				</td>
				<td style="background: #f7f7f7" class="field">
					<input class="extra-small-text" type="text" placeholder="1402" name="ids" id="ids" value="<?php echo dt_get_meta('ids'); ?>">
					<input class="extra-small-text" type="text" placeholder="1" name="temporada" id="temporada" value="<?php echo dt_get_meta('temporada'); ?>">
					<input type="button" class="button button-primary" id="generate_data_api" name="generate_data_api" value="<?php if(dt_get_meta('ids')){ _d('Update data'); } else { _d('Generate'); } ?>">
					<p class="description"><?php _d('E.g. https://www.themoviedb.org/tv/<strong>1402</strong>-the-walking-dead/season/<strong>1</strong>/'); ?></p>
					<p id="verificador" style="display:none"><a class="button button-secundary" id="comprovate"><?php _d('Check duplicate content'); ?></a><p>
				</td>
			</tr>
			<tr id="dt_episodes_box">
				<td class="label">
					<label><?php _d('Episodes control'); ?></label>
				</td>
				<td class="field">
					<p><input type="checkbox" name="clgnrt" id="clgnrt" value="1" <?php echo ( dt_get_meta('clgnrt') === '1') ? 'checked' : ''; ?>> <?php _d('I generated episodes or add manually'); ?></p>
				</td>
			</tr>
			<tr id="serie_name_box">
				<td class="label">
					<label><?php _d('Serie name'); ?></label>
				</td>
				<td class="field">
					<input class="regular-text" type="text" name="serie" id="serie" value="<?php echo dt_get_meta('serie'); ?>">
				</td>
			</tr>
			<tr id="dt_poster_box">
				<td class="label">
					<label><?php _d('Poster'); ?></label>
					<p class="description"><?php _d('Add url image'); ?></p>
				</td>
				<td class="field">
					<input class="regular-text" type="text" name="dt_poster" id="dt_poster" value="<?php echo dt_get_meta('dt_poster'); ?>">
					<input class="up_images_poster button-secondary" type="button" value="<?php _d('Upload'); ?>" />
				</td>
			</tr>

			<tr id="air_date_box">
				<td class="label">
					<label><?php _d('Air date'); ?></label>
				</td>
				<td class="field">
					<input class="small-text" type="date" name="air_date" id="air_date" value="<?php echo dt_get_meta('air_date'); ?>" required>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- ######################### -->
<?php if (has_post_thumbnail()): else: echo '<input type="hidden" id="url_image_upload" name="url_image_upload" value="">'; endif; ?>
<?php  }
function seasons_save( $post_id ) {
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
	if ( ! isset( $_POST['seasons_nonce'] ) || ! wp_verify_nonce( $_POST['seasons_nonce'], '_seasons_nonce') ) return;
	if ( ! current_user_can('edit_post', $post_id ) ) return;
	if ( isset( $_POST['ids'] ) ) update_post_meta( $post_id, 'ids', esc_attr( $_POST['ids'] ) );
	if ( isset( $_POST['temporada'] ) ) update_post_meta( $post_id, 'temporada', esc_attr( $_POST['temporada'] ) );
	if ( isset( $_POST['dt_poster'] ) ) update_post_meta( $post_id, 'dt_poster', esc_attr( $_POST['dt_poster'] ) );
	if ( isset( $_POST['serie'] ) ) update_post_meta( $post_id, 'serie', esc_attr( $_POST['serie'] ) );
	if ( isset( $_POST['air_date'] ) ) update_post_meta( $post_id, 'air_date', esc_attr( $_POST['air_date'] ) );
	if ( isset( $_POST['clgnrt'] ) ) update_post_meta( $post_id, 'clgnrt', esc_attr( $_POST['clgnrt'] ) ); else update_post_meta( $post_id, 'clgnrt', null );
	if (has_post_thumbnail()): else:  if($data = $_POST['url_image_upload']) {  dt_upload_image( $data,   $post_id ); } endif;
}
add_action('save_post', 'seasons_save');

function custom_admin_js_seasons() { 
global $post_type; if( $post_type == 'seasons') {	?>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery(".dtload").click(function() {
        var o = jQuery(this).attr("id");
        1 == o ? (jQuery(".dtloadpage").hide(), jQuery(this).attr("id", "0")) : (jQuery(".dtloadpage").show(), jQuery(this).attr("id", "1"))
    }), jQuery(".dtloadpage").mouseup(function() {
        return !1
    }), jQuery(".dtload").mouseup(function() {
        return !1
    }), jQuery(document).mouseup(function() {
        jQuery(".dtloadpage").hide(), jQuery(".dtload").attr("id", "")
    })
})
</script>
<div class="dtloadpage">
	<div class="dtloadbox">
		<img src="<?php echo get_template_directory_uri().'/assets/img/'; ?>admin_load.gif">
		<span><?php _d('Generating episodes'); ?></span>
		<p><?php _d('not close this page to complete the upload'); ?></p>
	</div>
</div>

<?php 
  } 
}
add_action('admin_footer', 'custom_admin_js_seasons');
