<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/

// Danh Mục
function dtdanhmuc() {
	$labels = array(
		'name'                       => __d('Danh Mục'),
		'singular_name'              => __d('Danh Mục'),
		'menu_name'                  => __d('Danh Mục'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_danh_muc_slug','danh_muc'),
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtdanhmuc', array('movies'), $args );
}
add_action('init', 'dtdanhmuc', 0 );

// Quốc Gia
function dtquocgia() {
	$labels = array(
		'name'                       => __d('Quốc Gia'),
		'singular_name'              => __d('Quốc Gia'),
		'menu_name'                  => __d('Quốc Gia'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_quoc_gia_slug','quoc_gia'),
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtquocgia', array('movies'), $args );
}
add_action('init', 'dtquocgia', 0 );

// Năm Phát Hành
function dtyear() {
	$labels = array(
		'name'                       => __d('Year'),
		'singular_name'              => __d('Year'),
		'menu_name'                  => __d('Year'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_release_slug','release'),
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtyear', array('tvshows','movies'), $args );
}
add_action('init', 'dtyear', 0 );

// Đạo Diễn
function dtdirector() {
	$labels = array(
		'name'                       => __d('Director'),
		'singular_name'              => __d('Director'),
		'menu_name'                  => __d('Director'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_director_slug','director'),
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtdirector', array('movies'), $args );
}
add_action('init', 'dtdirector', 0 );

// Diễn Viên
function dtcast() {
	$labels = array(
		'name'                       => __d('Cast'),
		'singular_name'              => __d('Cast'),
		'menu_name'                  => __d('Cast'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_cast_slug','cast'),
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtcast', array('tvshows','movies'), $args );
}
add_action('init', 'dtcast', 0 );

// Creator
function dtcreator() {
	$labels = array(
		'name'                       => __d('Creator'),
		'singular_name'              => __d('Creator'),
		'menu_name'                  => __d('Creator'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_creator_slug','creator'),
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtcreator', array('tvshows'), $args );
}
add_action('init', 'dtcreator', 0 );

// Studio
function dtstudio() {
	$labels = array(
		'name'                       => __d('Studio'),
		'singular_name'              => __d('Studio'),
		'menu_name'                  => __d('Studio'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_studio_slug','studio'),
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtstudio', array('tvshows'), $args );
}
add_action('init', 'dtstudio', 0 );

// Neworks
function dtnetworks() {
	$labels = array(
		'name'                       => __d('Networks'),
		'singular_name'              => __d('Networks'),
		'menu_name'                  => __d('Networks'),
	);
	$rewrite = array(
		'slug'                       => get_option('dt_network_slug','network'),
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy('dtnetworks', array('tvshows'), $args );
}
add_action('init', 'dtnetworks', 0 );