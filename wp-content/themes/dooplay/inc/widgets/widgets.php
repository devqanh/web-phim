<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 1.2.0
* @date: 2017-04-02 / 20:58:25
* @last modified by: Erick Meza
* @last modified time: 2017-04-02 / 23:15:19
*
*/

// Home
function sidebar_home()
{
	register_sidebar(array(
		'name' => __d('Sidebar Home Page') ,
		'id' => 'sidebar-home',
		'description' => __d('Add widgets here to appear in your sidebar.') ,
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'sidebar_home');

// Movies
function sidebar_movies()
{
	register_sidebar(array(
		'name' => __d('Sidebar Movies Single') ,
		'id' => 'sidebar-movies',
		'description' => __d('Add widgets here to appear in your sidebar.') ,
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'sidebar_movies');
// TVShows
function sidebar_tvshows()
{
	register_sidebar(array(
		'name' => __d('Sidebar TVShows Single') ,
		'id' => 'sidebar-tvshows',
		'description' => __d('Add widgets here to appear in your sidebar.') ,
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'sidebar_tvshows');
// Seasons
function sidebar_seasons()
{
	register_sidebar(array(
		'name' => __d('Sidebar Seasons Single') ,
		'id' => 'sidebar-seasons',
		'description' => __d('Add widgets here to appear in your sidebar.') ,
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'sidebar_seasons');

// Posts
function sidebar_posts()
{
	register_sidebar(array(
		'name' => __d('Sidebar Posts Single') ,
		'id' => 'sidebar-posts',
		'description' => __d('Add widgets here to appear in your sidebar.') ,
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h2 class="widget-title">',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'sidebar_posts');


// Home
function widgets_home()
{
	register_sidebar(array(
		'name' => __d('Home Page Genres') ,
		'id' => 'widgets-home',
		'description' => __d('Only widgets for the genres in home modules.') ,
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}
add_action('widgets_init', 'widgets_home');




// Registrar Widgets
function dt_widgets()
{
	register_widget('DT_Widget');
	register_widget('DT_Widget_views');
	register_widget('DT_Widget_social');
	register_widget('DT_Widget_related');
	register_widget('DT_Widget_genres');
	register_widget('DT_Widget_mgenres');
	register_widget('DT_Widget_mreleases');
}
add_action('widgets_init', 'dt_widgets');

get_template_part ('inc/widgets/content_widget');
get_template_part ('inc/widgets/content_related_widget');
get_template_part ('inc/widgets/content_widget_views');
get_template_part ('inc/widgets/content_widget_social');
get_template_part ('inc/widgets/content_widget_home');
get_template_part ('inc/widgets/content_widget_meta_genres');
get_template_part ('inc/widgets/content_widget_meta_releases');