jQuery(document).ready(function($) {

	function switch_tabs(a) {
		$(".tab-content").hide(), 
		$("#acera-main-menu ul a").removeClass("selected");
		var b = a.attr("rel");
		$("#" + b).fadeIn(500), a.addClass("selected")
	}

	function checked_img(a) {
		a.is(":checked") ? a.closest("label").addClass("acera-img-selected") : a.closest("label").removeClass("acera-img-selected")
	}

	function checked_img_radio(a) {
		a.is(":checked") ? (a.closest(".cOf").find("label.acera-img-selected").removeClass("acera-img-selected"), a.closest("label").addClass("acera-img-selected")) : a.closest("label").removeClass("acera-img-selected")
	}

	$.fn.slideFadeToggle = function(a, b, c) {
		return this.animate({
			opacity: "toggle",
			height: "toggle"
		}, a, b, c)
	}, 
	
	$("#acera-main-menu > li > p").click(function() {
		$(this).next().slideToggle(300)
	}), 
			
	$("#acera-main-menu ul a").click(function() {
		switch_tabs( $(this) )
	}), 
			
	$(".tab-content").hide();
	
	var a = $(".defaulttab").attr("rel");
	
	$("#" + a).show(), 
	$(".default-accordion").show(), 
	
	$(".acera-image-checkbox-b").click(function() {
		var a = $(this);
		checked_img(a)
	}), 
	
	$(".acera-image-radio-b").click(function() {
		var a = $(this);
		checked_img_radio(a)
	}), 

	$('input[type="checkbox"]').each(function() {
		var a = $(this),
			b = a.attr("id");
		a.is(":checked") || $('div[rel="' + b + '"]').hide()
	}), 
	
	$('input[type="checkbox"]').click(function() {
		var a = $(this),
			b = a.attr("id");
		a.is(":checked") ? $('div[rel="' + b + '"]').slideFadeToggle(500) : $('div[rel="' + b + '"]').slideFadeToggle(500)
	}), 
	
	// Upload image
    $('.acera_upload').click(function(e) {
		var custom_uploader;
		var id = $(this).attr('data-id');
        e.preventDefault();
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: dtfa.ititle,
            button: {
                text: dtfa.iuse
            },
            multiple: false
        });
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#' + id ).val(attachment.url);
			$('#preview_' + id ).html('<div data-id="'+ id +'" class="img upload_image"><img src="'+ attachment.url +'"></div>');
			$('#remove_' + id ).removeClass( "hiddenex" );
			var uploading = {
				action: "dtfr_ajax_upload",
				data: id,
				url: attachment.url,
			}
			$.post( ajaxurl, uploading );

        });
        custom_uploader.open();
   }),
			
	$(".acera_remove").live("click", function() {
		var a = $(this),
			b = $(this).attr('data-id');
		a.html( dtfa.remov );
		var c = {
			action: "dtfr_ajax_remove",
			data: b
		};

		$.post( ajaxurl, c, function(b) {
			a.prev().prev().val(""), a.next().html(""), a.remove()
		})
	})
}),


function(a, b, c, d, e, f, g) {
	a.GoogleAnalyticsObject = e, a[e] = a[e] || function() {
		(a[e].q = a[e].q || []).push(arguments)
	}, a[e].l = 1 * new Date, f = b.createElement(c), g = b.getElementsByTagName(c)[0], f.async = 1, f.src = d, g.parentNode.insertBefore(f, g)
}(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga"), ga("create", "UA-xxxxxxxx-x", "auto"), ga("send", "pageview");