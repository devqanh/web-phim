jQuery(document).ready(function(a) {
    var t = DTapi.dbm + "/app/",
        i = DTapi.dbmkey;
    a("ul.tabs li").click(function() {
        var t = a(this).attr("data-tab");
        a("ul.tabs li").removeClass("current"), a(".tab-content").removeClass("current"), a(this).addClass("current"), a("#" + t).addClass("current")
    }), a("#search_imdb").submit(function() {
        if (a("#resultado").html('<div class="content"><span class="spinner"></span> ' + DTapi.preresultado + "</div>"), "1" == DTapi.apk) {
            a("input[name=imdbyear]").val();
            var t = a("input[name=imdbpage]").val();
            console.log(DTapi.preresultadolog), a("input[name=search_data_imdb]").prop("disabled", !0), a("input[name=search_data_imdb]").val(DTapi.loading), a.ajax({
                type: "POST",
                url: DTapi.ajaxurl + "?action=dbm_get_movies",
                data: a(this).serialize()
            }).done(function(i) {
                console.log(DTapi.resultadolog), a("input[name=imdbpage]").val(parseFloat(t) + parseFloat(1)), a("#resultado").html('<div class="content">' + i + "</div>"), a("input[name=search_data_imdb]").prop("disabled", !1), a("input[name=search_data_imdb]").val(DTapi.getcontent), a(".a_import_imdb").click(function(t) {
                    var i = a(this).data("id"),
                        e = "#" + i;
                    a("input[name=idmovie]").val(i), a("input[name=send_id_movie]").trigger("click"), a(e).hide("fast")
                }), a("#load_more_imdb_link").click(function() {
                    a("input[name=search_data_imdb]").trigger("click"), a("#filter_year_li").trigger("click")
                })
            }).fail(function(t) {
                console.log(DTapi.resultadoerror), a("#resultado").html('<div class="content">' + DTapi.resultadoerror + "</div>")
            })
        }
        return !1
    }), a("#search_all").submit(function() {
        if (a("#resultado").html('<div class="content"><span class="spinner"></span> ' + DTapi.preresultado + "</div>"), "1" == DTapi.apk) {
            var t = a("input[name=page]").val();
            a.ajax({
                type: "POST",
                url: DTapi.ajaxurl + "?action=dbm_search_all",
                data: a(this).serialize()
            }).done(function(i) {
                a("input[name=page]").val(parseFloat(t) + parseFloat(1)), a("#resultado").html('<div class="content">' + i + "</div>"), a(".a_import_imdb").click(function(t) {
                    var i = a(this).data("id"),
                        e = "#" + i;
                    a("input[name=idmovie]").val(i), a("input[name=send_id_movie]").trigger("click"), a(e).hide("fast")
                }), a(".a_import_tmdb").click(function(t) {
                    var i = a(this).data("id"),
                        e = "#" + i;
                    a("input[name=idtv]").val(i), a("input[name=send_id_tv]").trigger("click"), a(e).hide("fast")
                }), a("#load_more_search").click(function() {
                    a("button[name=search_all_data").trigger("click")
                })
            }).fail(function(t) {
                console.log(DTapi.resultadoerror), a("#resultado").html('<div class="content">' + DTapi.resultadoerror + "</div>")
            })
        }
        return !1
    }), a("#single_url_imdb").submit(function() {
        if (a("#add_data_post").html('<p><span class="spinner"></span> ' + DTapi.agregandodatos + "</p>"), "1" == DTapi.apk) {
            a("input[name=idmovie]").val();
            console.log(DTapi.agregandodatoslog), a.ajax({
                type: "POST",
                url: DTapi.ajaxurl + "?action=dbm_post_movie",
                data: a(this).serialize()
            }).done(function(t) {
                a("input[name=idmovie]").val(""), a("#add_data_post").html("<p>" + t + "</p>"), console.log(DTapi.procesocompleto)
            }).fail(function(t) {
                console.log(DTapi.postdataerror), a("#add_data_post").html("<p>" + DTapi.postdataerror + "</p>")
            })
        }
        return !1
    }), a("#search_tmdb").submit(function() {
        if (a("#resultado").html('<div class="content"><span class="spinner"></span> ' + DTapi.preresultado + "</div>"), "1" == DTapi.apk) {
            a("input[name=tmdbyear]").val();
            var t = a("input[name=tmdbpage]").val();
            console.log(DTapi.preresultadolog), a("input[name=search_data_tmdb]").prop("disabled", !0), a("input[name=search_data_tmdb]").val(DTapi.loading), a.ajax({
                type: "POST",
                url: DTapi.ajaxurl + "?action=dbm_get_tv",
                data: a(this).serialize()
            }).done(function(i) {
                a("input[name=tmdbpage]").val(parseFloat(t) + parseFloat(1)), console.log(DTapi.resultadolog), a("#resultado").html('<div class="content">' + i + "</div>"), a("input[name=search_data_tmdb]").prop("disabled", !1), a("input[name=search_data_tmdb]").val(DTapi.getcontent), a(".a_import_tmdb").click(function(t) {
                    var i = a(this).data("id"),
                        e = "#" + i;
                    a("input[name=idtv]").val(i), a("input[name=send_id_tv]").trigger("click"), a(e).hide("fast")
                }), a("#load_more_tmdb_link").click(function() {
                    a("input[name=search_data_tmdb]").trigger("click"), a("#filter_year_li").trigger("click")
                })
            }).fail(function(t) {
                console.log(DTapi.postdataerror), a("#resultado").html('<div class="content">' + DTapi.postdataerror + "</div>")
            })
        }
        return !1
    }), a("#single_url_tmdb").submit(function() {
        if (a("#add_data_post").html('<p><span class="spinner"></span> ' + DTapi.agregandodatos + "</p>"), "1" == DTapi.apk) {
            a("input[name=idtv]").val();
            console.log(DTapi.agregandodatoslog), a.ajax({
                type: "POST",
                url: DTapi.ajaxurl + "?action=dbm_post_tv",
                data: a(this).serialize()
            }).done(function(t) {
                a("input[name=idtv]").val(""), a("#add_data_post").html("<p>" + t + "</p>"), console.log(DTapi.procesocompleto)
            }).fail(function(t) {
                console.log(DTapi.postdataerror), a("#add_data_post").html("<p>" + DTapi.postdataerror + "</p>")
            })
        }
        return !1
    }), a("#api_status").submit(function() {
        return a("#add_data_post").html('<p><span class="spinner"></span> ' + DTapi.queryserver + "</p>"), "1" == DTapi.apk && (console.log(DTapi.preresultadolog), a.ajax({
            type: "POST",
            url: DTapi.ajaxurl + "?action=dbm_status",
            data: a(this).serialize()
        }).done(function(t) {
            console.log(DTapi.resultadolog), a("#add_data_post").html("<p>" + DTapi.verificationsr + "</p>"), a("#result_server").html(t), a(".skillbar").each(function() {
                a(this).find(".skillbar-bar").animate({
                    width: a(this).attr("data-percent")
                }, 500)
            })
        }).fail(function(t) {
            console.log(DTapi.postdataerror), a("#result_server").html(DTapi.postdataerror)
        })), !1
    }), a(".register_api").click(function() {
        var t = dooAj.url;
        return a("#add_data_post").html('<p><span class="spinner"></span> ' + DTapi.loading + "</p>"), a.ajax({
            url: t,
            type: "post",
            data: {
                action: "register_site_db"
            },
            error: function(a) {
                console.log(a)
            },
            success: function(t) {
                1 == t && (a("#add_data_post").html("<p>" + DTapi.procesocompleto + "</p>"), location.reload()), 2 == t && (a("#add_data_post").html("<p>" + DTapi.procesocompleto + "</p>"), location.reload())
            }
        }), !1
    }), a.getJSON(t + i, function(t) {
        a.each(t, function(i, e) {
            "response" == i && "0" == e && a.each(t, function(t, i) {
                "message" == t && a(".dt_importer_contaiter").html('<div class="my_key">' + i + "</div>")
            })
        })
    }), a("input[name=imdbyear]").click(function() {
        a("input[name=imdbpage]").val("1")
    }), a("input[name=tmdbyear]").click(function() {
        a("input[name=tmdbpage]").val("1")
    }), a("input[name=query]").click(function() {
        a("input[name=page]").val("1")
    })
});