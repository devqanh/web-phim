jQuery(document).ready(function($) {	
	// Registrar API
	$('.activate_dbmovies').click(function(){
        var ajaxurl = dooAj.url;
		$('#ac_dbm_not').html( dooAj.loading )
		$.ajax({
            url: ajaxurl,
            type: 'post',
			data: {
                action: 'dbmv_api_register'
            },
			error: function(response) {
                console.log(response);
            },
			success: function( response ) {
				if (response == 1) {
					$('#ac_dbm_not').html( dooAj.reloading )
					location.reload();
				}

				if(response == 2) {
					$('#ac_dbm_not').html( dooAj.reloading )
					location.reload();
				}
				
            }
		});
		return false;
	});

	// Update database
	$('.dooplay_update_database').click(function(){
        var ajaxurl = dooAj.url;
		var action = $(this).data('action');
		$('#cfg_dts').html( dooAj.updb )
		$.ajax({
			action: action,
            url: ajaxurl,
            type: 'post',
			data: {
                action: 'update_dbdooplay'
            },
			error: function(response) {
                console.log(response);
            },
			success: function( response ) {
				location.reload();
            }
		});
		return false;
	});

	// Save dbmovies settings
	$(document).on('submit', '#dbmovies_settings', function() {
		var that = $(this);
		var ajaxurl = DTapi.ajaxurl;
		$('#add_data_post').html('<p><span class="spinner"></span> '+ DTapi.saving +'</p>')
		$('.perico').addClass( "saving" )
		$('#save_sdbmvs').prop('disabled', true)
		$('#save_sdbmvs').val( DTapi.saving )
		$.ajax({
            url: ajaxurl,
            type: 'post',
            data: that.serialize(),
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
				$('#add_data_post').html('')
				$('.perico').removeClass( 'saving' )
				$('#save_sdbmvs').prop('disabled', false)
		        $('#save_sdbmvs').val( DTapi.save )
            },
        });

		return false;
	});
});