jQuery(document).ready(function(e) {
    e("#generate_data_api").click(function() {
        var a = e("#ids").get(0).value,
            i = e("#temporada").get(0).value,
            t = e("#episodio").get(0).value,
            n = DTapi.dbm + "/app/",
            p = DTapi.dbmkey,
            l = "&api_key=" + DTapi.tmdkey,
            s = "&language=" + DTapi.lang + "&include_image_language=" + DTapi.lang + ",null",
            o = DTapi.tmd,
            d = DTapi.pda;
                "1" == d ? e.getJSON(o + a + "/season/" + i + "/episode/" + t + "?append_to_response=images" + s + l, function(n) {
                    e("#loading_api").html(""), e("#api_table").removeClass("hidden_api");
                    var p = "",
                        s = "";
                    e.each(n, function(d, g) {
                        if (e("input[name=" + d + "]").val(g), e("#message").remove(), e("#verificador").show(), e("#dt_backdrop").val(p), "vote_count" == d && e("#serie_vote_count").val(g), "id" == d && e("#dt_string").val("tv" + DTapi.string + g), "name" == d && e("#episode_name").val(g), "vote_average" == d && e("#serie_vote_average").val(g), "overview" == d && "undefined" != typeof tinymce) {
                            var r = tinymce.get("content");
                            r && r instanceof tinymce.Editor ? (r.setContent(g), r.save({
                                no_events: !0
                            })) : e("textarea#content").val(g)
                        }
                        if ("still_path" == d && (p += g + ""), "still_path" == d && (s += "https://image.tmdb.org/t/p/w396" + g, "true" == DTapi.upload && ("edit" == DTapi.post || (e("#url_image_upload").val(s), e("#postimagediv p").html("<ul><li><img src='" + s + "'/> </li></ul>")))), "images" == d) {
                            var c = "";
                            e.each(n.images.stills, function(e, a) {
                                if (e > 9) return !1;
                                c += a.file_path + "\n"
                            }), e('textarea[name="imagenes"]').val(c)
                        }
                        e.getJSON(o + a + "?language=" + DTapi.lang + "&include_image_language=" + DTapi.lang + ",null" + l, function(a) {
                            e.each(a, function(a, n) {
                                "name" == a && (e("#serie").val(n), e("label#title-prompt-text").addClass("screen-reader-text"), e("input[name=post_title]").val(n + ": " + DTapi.eseas + i + DTapi.esepart + DTapi.eepisod + t))
                            })
                        }), e.getJSON(o + a + "/season/" + i + "?language=" + DTapi.lang + l, function(a) {
                            e.each(a, function(a, i) {
                                "poster_path" == a && e("#dt_poster").val(i)
                            })
                        })
                    })
                }) : e.each(n, function(a, i) {
                    "message" == a && (e("#postbox-container-2").prepend('<div id="message" class="notice notice-error"><p>' + i + "</p></div>"), e("#loading_api").html(""), e("#api_table").removeClass("hidden_api"))
                })

    })
});