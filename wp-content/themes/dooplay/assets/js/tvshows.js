jQuery(document).ready(function(a) {
    a("#generate_data_api").click(function() {
        var e = a("#ids").get(0).value,
            t = DTapi.dbm + "/app/",
            n = DTapi.tmd,
            i = DTapi.dbmkey,
            o = "&api_key=" + DTapi.tmdkey,
            p = "&language=" + DTapi.lang + "&include_image_language=" + DTapi.lang + ",null",
            r = (DTapi.genres, DTapi.upload),
            s = DTapi.pda;


                "1" == s ? a.getJSON(n + e + "?append_to_response=images,trailers" + p + o, function(t) {
                    var i = "";
                    a("#loading_api").html(""), a("#api_table").removeClass("hidden_api"), a.each(t, function(p, s) {
                        if (a("input[name=" + p + "]").val(s), a("#message").remove(), a("#verificador").show(), "name" == p && (a("label#title-prompt-text").addClass("screen-reader-text"), a("input[name=post_title]").val(s)), "vote_count" == p && a("#imdbVotes").val(s), "vote_average" == p && a("#imdbRating").val(s), "overview" == p && "undefined" != typeof tinymce) {
                            var c = tinymce.get("content");
                            c && c instanceof tinymce.Editor ? (c.setContent(s), c.save({
                                no_events: !0
                            })) : a("textarea#content").val(s)
                        }
                        if ("poster_path" == p && a('input[name="dt_poster"]').val(s), "backdrop_path" == p && a('input[name="dt_backdrop"]').val(s), "poster_path" == p && (i += "https://image.tmdb.org/t/p/w396" + s, "true" == r && ("edit" == DTapi.post || (a("#url_image_upload").val(i), a("#postimagediv p").html("<ul><li><img class='dt_poster_preview' src='" + i + "'/> </li></ul>")))), "images" == p) {
                            var d = "";
                            a.each(t.images.backdrops, function(a, e) {
                                if (a > 9) return !1;
                                d += e.file_path + "\n"
                            }), a('textarea[name="imagenes"]').val(d)
                        }
                        if ("first_air_date" == p && a("#new-tag-dtyear").val(s.slice(0, 4)), "created_by" == p) {
                            var l = "",
                                u = "";
                            a.each(t.created_by, function(a, e) {
                                l += e.name + ",", u += "[" + e.profile_path + ";" + e.name + "]"
                            }), a("#new-tag-dtcreator").val(l), a("#dt_creator").val(u)
                        }
                        if ("production_companies" == p) {
                            var m = "";
                            a.each(t.production_companies, function(a, e) {
                                m += e.name + ","
                            }), a("#new-tag-dtstudio").val(m)
                        }
                        if ("networks" == p) {
                            var v = "";
                            a.each(t.networks, function(a, e) {
                                v += e.name + ","
                            }), a("#new-tag-dtnetworks").val(v)
                        }
                        a.getJSON(n + e + "/credits?" + o, function(e) {
                            a.each(e, function(t, n) {
                                if ("cast" == t) {
                                    var i = "",
                                        o = "";
                                    a.each(e.cast, function(a, e) {
                                        if (a > 9) return !1;
                                        i += e.name + ", ", o += "[" + e.profile_path + ";" + e.name + "," + e.character + "]"
                                    }), a("#new-tag-dtcast").val(i), a("#dt_cast").val(o)
                                }
                            })
                        }), a.getJSON(n + e + "/videos?language=" + DTapi.lang + o, function(e) {
                            a.each(e, function(t, n) {
                                var i = "";
                                a.each(e.results, function(a, e) {
                                    if (a > 0) return !1;
                                    i += "[" + e.key + "]"
                                }), a('input[name="youtube_id"]').val(i)
                            })
                        })
                    })
                }) : a.each(t, function(e, t) {
                    "message" == e && (a("#postbox-container-2").prepend('<div id="message" class="notice notice-error"><p>' + t + "</p></div>"), a("#loading_api").html(""), a("#api_table").removeClass("hidden_api"))
                })
    })
});