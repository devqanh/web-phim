jQuery(document).ready(function(e) {
    e("#generate_data_api").click(function() {
        var a = e("#ids").get(0).value,
            t = e("#temporada").get(0).value,
            i = DTapi.dbm + "/app/",
            n = DTapi.dbmkey,
            p = "&api_key=" + DTapi.tmdkey,
            o = "&language=" + DTapi.lang + "&include_image_language=" + DTapi.lang + ",null",
            s = DTapi.tmd,
            l = DTapi.pda;
                "1" == l ? e.getJSON(s + a + "/season/" + t + "?append_to_response=images" + o + p, function(i) {
                    e("#loading_api").html(""), e("#api_table").removeClass("hidden_api");
                    var n = "";
                    e.each(i, function(i, o) {
                        if (e("input[name=" + i + "]").val(o), e("#message").remove(), e("#verificador").show(), "overview" == i && "undefined" != typeof tinymce) {
                            var l = tinymce.get("content");
                            l && l instanceof tinymce.Editor ? (l.setContent(o), l.save({
                                no_events: !0
                            })) : e("textarea#content").val(o)
                        }
                        "name" == i && (n += "" + o), e.getJSON(s + a + "?language=" + DTapi.lang + p, function(a) {
                            e.each(a, function(a, i) {
                                "name" == a && (e("#serie").val(i), e("label#title-prompt-text").addClass("screen-reader-text"), e("input[name=post_title]").val(i + ": " + DTapi.slug + " " + t))
                            })
                        }), e.getJSON(s + a + "/season/" + t + "?language=" + DTapi.lang + p, function(a) {
                            e.each(a, function(a, t) {
                                "poster_path" == a && (e("#dt_poster").val(t), "true" == DTapi.upload && ("edit" == DTapi.post || (e("#url_image_upload").val("https://image.tmdb.org/t/p/w396" + t), e("#postimagediv p").html("<ul><li><img class='dt_poster_preview' src='https://image.tmdb.org/t/p/w396" + t + "'/> </li></ul>"))))
                            })
                        })
                    })
                }) : e.each(i, function(a, t) {
                    "message" == a && (e("#postbox-container-2").prepend('<div id="message" class="notice notice-error"><p>' + t + "</p></div>"), e("#loading_api").html(""), e("#api_table").removeClass("hidden_api"))
                })

    })
});