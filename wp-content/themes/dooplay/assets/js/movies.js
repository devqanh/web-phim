jQuery(document).ready(function(e) {
    e("#generate_data_api").click(function() {
        var a = e("#ids").get(0).value,
            t = DTapi.dbm,
            i = DTapi.tmd,
            n = DTapi.dbmkey,
            r = "&api_key=" + DTapi.tmdkey,
            l = "&language=" + DTapi.lang + "&include_image_language=" + DTapi.lang + ",null",
            p = (DTapi.genres, DTapi.upload),
            s = DTapi.pda;
        e.getJSON(t + "/" + a, function(t) {
e("#imdbRating").val(t.imdbRating), e("#imdbVotes").val(t.imdbVotes), e("#Rated").val(t.Rated), e("#Country").val(t.Country)
               e.getJSON(i + a + "?append_to_response=images,trailers" + l + r, function(t) {
                    var n = "";
                    e("#loading_api").html(""), e("#api_table").removeClass("hidden_api"), e.each(t, function(l, s) {
                        if (e("input[name=" + l + "]").val(s), e("#message").remove(), e("#verificador").show(), "title" == l && (e("label#title-prompt-text").addClass("screen-reader-text"), e("input[name=post_title]").val(s)), "id" == l && e("#dt_string").val("mov" + DTapi.string + s), "overview" == l && "undefined" != typeof tinymce) {
                            var d = tinymce.get("content");
                            d && d instanceof tinymce.Editor ? (d.setContent(s), d.save({
                                no_events: !0
                            })) : e("textarea#content").val(s)
                        }
                        if ("poster_path" == l && e('input[name="dt_poster"]').val(s), "poster_path" == l && "true" == p && ("edit" == DTapi.post || (n += "https://image.tmdb.org/t/p/w396" + s, e("#url_image_upload").val(n), e("#postimagediv p").html("<ul><li><img class='dt_poster_preview' src='" + n + "'/> </li></ul>"))), "backdrop_path" == l && e('input[name="dt_backdrop"]').val(s), "id" == l && e('input[name="idtmdb"]').val(s), "release_date" == l && e("#new-tag-dtyear").val(s.slice(0, 4)), "trailers" == l) {
                            var c = "";
                            e.each(t.trailers.youtube, function(e, a) {
                                if (e > 0) return !1;
                                c += "[" + a.source + "]"
                            }), e('input[name="youtube_id"]').val(c)
                        }
                        if ("images" == l) {
                            var o = "";
                            e.each(t.images.backdrops, function(e, a) {
                                if (e > 9) return !1;
                                o += a.file_path + "\n"
                            }), e('textarea[name="imagenes"]').val(o)
                        }
                        e.getJSON(i + a + "/credits?" + r, function(a) {
                            e.each(a, function(t, i) {
                                if ("cast" == t) {
                                    var n = cstml = "";
                                    e.each(a.cast, function(e, a) {
                                        if (e > 9) return !1;
                                        n += "[" + a.profile_path + ";" + a.name + "," + a.character + "]", cstml += a.name + ", "
                                    }), e('textarea[name="dt_cast"]').val(n);
                                    var r = "";
                                    e.each(a.cast, function(e, a) {
                                        if (e > 9) return !1;
                                        r += a.name + ", "
                                    }), e("#new-tag-dtcast").val(r)
                                } else {
                                    var l = crew_dl = "";
                                    crew_wl = "";
                                    e.each(a.crew, function(e, a) {
                                        "Directing" == a.department && (l += "[" + a.profile_path + ";" + a.name + "]", crew_dl += a.name + ", ")
                                    }), e("input[name=dt_dir]").val(l), e("#new-tag-dtdirector").val(crew_dl)
                                }
                            })
                        })
                    })
			   } )

        })
    })
});