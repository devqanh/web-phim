<?php
/* 
* -------------------------------------------------------------------------------------
* @author: emeza
* @author URI: https://doothemes.com/
* @copyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.0.4
*
*/
get_header();
get_template_part('inc/dt_slider'); 
// end header home
?>

<?php echo do_shortcode('[rev_slider alias="movie"]'); ?>

<div class="module">
	<div class="content">
	
		<?php get_template_part('inc/parts/modules/phimdecu'); ?>
		<?php get_template_part('inc/parts/modules/phimchieurap'); ?>
		<?php get_template_part('inc/parts/modules/phimhoathinh'); ?>
		<?php get_template_part('inc/parts/modules/phimvietnam'); ?>
		<?php get_template_part('inc/parts/modules/phimle'); ?>
		<?php get_template_part('inc/parts/modules/phim18'); ?>
		<?php //get_template_part('inc/parts/modules/phimbo'); ?>
		
		<?php $codex = get_option('dt_shorcode_home'); if($codex):
			do_shortcode($codex); 
			else:
			 get_template_part('inc/parts/modules/slider');
			 get_template_part('inc/parts/modules/letter');
			 get_template_part('inc/parts/modules/movies');
			 get_template_part('inc/parts/modules/ads');
			 get_template_part('inc/parts/modules/tvshows');
			 get_template_part('inc/parts/modules/seasons');
			 get_template_part('inc/parts/modules/episodes');
			 get_template_part('inc/parts/modules/top-imdb');
			 get_template_part('inc/parts/modules/blog');
			endif;
		?>
		
	</div>
	<div class="sidebar scrolling">
		<div class="fixed-sidebar-blank">
			<?php $widgets = dynamic_sidebar('sidebar-home'); if($widgets) { $widgets; } else { echo '<a href="'. esc_url( home_url() ) .'/wp-admin/widgets.php">'. __d('Add widgets') .'</a>'; } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>