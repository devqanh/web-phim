<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hdviet');
/** MySQL database username */
define('DB_USER', 'root');
/** MySQL database password */
define('DB_PASSWORD', '');
/** MySQL hostname */
define('DB_HOST', 'localhost');
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');
/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'hXS7;kRJV%FVc*$wcW]Qjl6~-Pz[2MrYH>w4WZXey{+;D/ea2g&!VJ~{kO9)ZQDm');
define('SECURE_AUTH_KEY',  'l:4lH`J]F)py>:TH064uTHjX8BU7@_bVlR]r5 9DGx@jR${e9u*na;_pVM2P(*9c');
define('LOGGED_IN_KEY',    'q^|?@Sj]M+d8NOGkX=s<q;xKU0O[%c</Rmgl ~%pbm~<pibO3%HZ1g.,TK|0xim|');
define('NONCE_KEY',        '1dHXvkK=6*]uAqBFI8s#]l@6uQEl%+|w?q?l6Hn@P>ZalDeg:<s0.bDxbKFeM`AB');
define('AUTH_SALT',        'Q?gw!SdK7BKWU*_O|hGon!EnQ7ovpW&_4^`OMNm@=K{8d/S=_<ute<nT?10sy*OJ');
define('SECURE_AUTH_SALT', 'W->vL3gSAZva0uE:}*u|Zg S41` aV@/|?D,miORoXD/:6jz]7S$*N$V.T*mLU&6');
define('LOGGED_IN_SALT',   'bj{To/_b<-.`MgO[to}Io|HSKrFyH-N3 Tg//,G&)35Vu)3?qjZ!$WA$z9Fo/TH&');
define('NONCE_SALT',       'GSg25-l#-%W9uqQ@aAO&q.nevX)Z@Nc:;r>T[W09??>}4mlW4?pG}Y5{vZ_c_`-U');
/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
